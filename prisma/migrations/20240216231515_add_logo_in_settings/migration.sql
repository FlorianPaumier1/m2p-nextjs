/*
  Warnings:

  - A unique constraint covering the columns `[slug]` on the table `blog` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[title]` on the table `blog` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[logo_id]` on the table `configuration` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `logo_id` to the `configuration` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "configuration" ADD COLUMN     "logo_id" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "blog_slug_key" ON "blog"("slug");

-- CreateIndex
CREATE UNIQUE INDEX "blog_title_key" ON "blog"("title");

-- CreateIndex
CREATE UNIQUE INDEX "configuration_logo_id_key" ON "configuration"("logo_id");

-- AddForeignKey
ALTER TABLE "configuration" ADD CONSTRAINT "fk_7161e18773f32dd8" FOREIGN KEY ("logo_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;
