/*
  Warnings:

  - Added the required column `sort` to the `home_about` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "home_about" ADD COLUMN     "sort" INTEGER NOT NULL;
