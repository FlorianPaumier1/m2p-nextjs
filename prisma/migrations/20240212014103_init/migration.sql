-- CreateTable
CREATE TABLE "about" (
    "uuid" TEXT NOT NULL,
    "banner_id" TEXT NOT NULL,
    "content" TEXT,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "about_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "about_action" (
    "uuid" TEXT NOT NULL,
    "image_id" TEXT NOT NULL,
    "home_id" TEXT NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "content" TEXT NOT NULL,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "about_action_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "about_item" (
    "uuid" TEXT NOT NULL,
    "about_id" TEXT NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "content" VARCHAR(255) NOT NULL,

    CONSTRAINT "about_item_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "about_item_date" (
    "uuid" TEXT NOT NULL,
    "about_item_id" TEXT NOT NULL,
    "title" VARCHAR(50) NOT NULL,
    "content" VARCHAR(255),

    CONSTRAINT "about_item_date_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "blog" (
    "uuid" TEXT NOT NULL,
    "thumbnail_id" TEXT NOT NULL,
    "banner_id" TEXT NOT NULL,
    "slug" VARCHAR(255) NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "content" TEXT NOT NULL,
    "caption" TEXT NOT NULL,
    "published" BOOLEAN NOT NULL DEFAULT false,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "blog_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "configuration" (
    "uuid" TEXT NOT NULL,
    "maintenance" BOOLEAN,
    "title" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "configuration_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "contact" (
    "uuid" TEXT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "subject" VARCHAR(255) NOT NULL,
    "message" TEXT NOT NULL,

    CONSTRAINT "contact_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "doctrine_migration_versions" (
    "version" VARCHAR(191) NOT NULL,
    "executed_at" TIMESTAMP(0),
    "execution_time" INTEGER,

    CONSTRAINT "doctrine_migration_versions_pkey" PRIMARY KEY ("version")
);

-- CreateTable
CREATE TABLE "file" (
    "uuid" TEXT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "size" INTEGER NOT NULL,
    "mime_type" VARCHAR(100) NOT NULL,
    "is_processed" BOOLEAN NOT NULL,
    "type" VARCHAR(20) NOT NULL,
    "alt" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "file_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "home" (
    "uuid" TEXT NOT NULL,
    "banner_id" TEXT NOT NULL,
    "about_id" TEXT NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "caption" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "home_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "home_about" (
    "uuid" TEXT NOT NULL,
    "home_id" TEXT NOT NULL,
    "about_title" VARCHAR(255) NOT NULL,

    CONSTRAINT "home_about_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "menu" (
    "uuid" TEXT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "label" VARCHAR(255) NOT NULL,
    "icon" VARCHAR(255) NOT NULL,
    "path" VARCHAR(255) NOT NULL,
    "enabled" BOOLEAN DEFAULT false,
    "sort" INTEGER NOT NULL DEFAULT 0,

    CONSTRAINT "menu_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "palmares" (
    "uuid" TEXT NOT NULL,
    "thumbnail_id" TEXT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "date" DATE NOT NULL,
    "description" TEXT NOT NULL,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "palmares_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "social" (
    "uuid" TEXT NOT NULL,
    "configuration_id" TEXT NOT NULL,
    "url" VARCHAR(255) NOT NULL,
    "icon" VARCHAR(255) NOT NULL,
    "name" VARCHAR(255) NOT NULL,

    CONSTRAINT "social_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "sponsor" (
    "uuid" TEXT NOT NULL,
    "thumbnail_id" TEXT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "date" DATE NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "sponsor_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "tag" (
    "uuid" TEXT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "tag_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "tag_blog" (
    "tag_id" TEXT NOT NULL,
    "blog_id" TEXT NOT NULL,

    CONSTRAINT "tag_blog_pkey" PRIMARY KEY ("tag_id","blog_id")
);

-- CreateTable
CREATE TABLE "team" (
    "uuid" TEXT NOT NULL,
    "thumbnail_id" TEXT NOT NULL,
    "full_name" VARCHAR(255) NOT NULL,
    "position" VARCHAR(255) NOT NULL,
    "sort" INTEGER NOT NULL,

    CONSTRAINT "team_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "thumbnail" (
    "uuid" TEXT NOT NULL,
    "src" VARCHAR(255) NOT NULL,
    "image_name" VARCHAR(255) NOT NULL,
    "alt" VARCHAR(255) NOT NULL,
    "height" DOUBLE PRECISION,
    "width" DOUBLE PRECISION,
    "created_at" TIMESTAMP(0),
    "updated_at" TIMESTAMP(0),

    CONSTRAINT "thumbnail_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "user" (
    "id" TEXT NOT NULL,
    "email" VARCHAR(180) NOT NULL,
    "password" VARCHAR(255) NOT NULL,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "account" (
    "_id" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "provider" TEXT NOT NULL,
    "providerAccountId" TEXT NOT NULL,
    "token_type" TEXT,
    "refresh_token" TEXT,
    "access_token" TEXT,
    "id_token" TEXT,
    "scope" TEXT,
    "session_state" TEXT,
    "expires_at" INTEGER,
    "userId" TEXT NOT NULL,

    CONSTRAINT "account_pkey" PRIMARY KEY ("_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "uniq_b5f422e3684ec833" ON "about"("banner_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_51cb3b933da5256d" ON "about_action"("image_id");

-- CreateIndex
CREATE INDEX "idx_51cb3b9328cdc89c" ON "about_action"("home_id");

-- CreateIndex
CREATE INDEX "idx_c0e0e4ed087db59" ON "about_item"("about_id");

-- CreateIndex
CREATE INDEX "idx_568cb47c657e657f" ON "about_item_date"("about_item_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_c0155143fdff2e92" ON "blog"("thumbnail_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_c0155143684ec833" ON "blog"("banner_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_71d60cd0684ec833" ON "home"("banner_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_71d60cd0d087db59" ON "home"("about_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_be0edb7328cdc89c" ON "home_about"("home_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_ff4ee649fdff2e92" ON "palmares"("thumbnail_id");

-- CreateIndex
CREATE INDEX "idx_7161e18773f32dd8" ON "social"("configuration_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_818cc9d4fdff2e92" ON "sponsor"("thumbnail_id");

-- CreateIndex
CREATE INDEX "idx_2e1aeef5bad26311" ON "tag_blog"("tag_id");

-- CreateIndex
CREATE INDEX "idx_2e1aeef5dae07e97" ON "tag_blog"("blog_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_c4e0a61ffdff2e92" ON "team"("thumbnail_id");

-- CreateIndex
CREATE UNIQUE INDEX "uniq_8d93d649e7927c74" ON "user"("email");

-- CreateIndex
CREATE UNIQUE INDEX "account_provider_providerAccountId_key" ON "account"("provider", "providerAccountId");

-- AddForeignKey
ALTER TABLE "about" ADD CONSTRAINT "fk_b5f422e3684ec833" FOREIGN KEY ("banner_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "about_action" ADD CONSTRAINT "fk_51cb3b9328cdc89c" FOREIGN KEY ("home_id") REFERENCES "home"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "about_action" ADD CONSTRAINT "fk_51cb3b933da5256d" FOREIGN KEY ("image_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "about_item" ADD CONSTRAINT "fk_c0e0e4ed087db59" FOREIGN KEY ("about_id") REFERENCES "home_about"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "about_item_date" ADD CONSTRAINT "fk_568cb47c657e657f" FOREIGN KEY ("about_item_id") REFERENCES "about_item"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "blog" ADD CONSTRAINT "fk_c0155143684ec833" FOREIGN KEY ("banner_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "blog" ADD CONSTRAINT "fk_c0155143fdff2e92" FOREIGN KEY ("thumbnail_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "home" ADD CONSTRAINT "fk_71d60cd0684ec833" FOREIGN KEY ("banner_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "home" ADD CONSTRAINT "fk_71d60cd0d087db59" FOREIGN KEY ("about_id") REFERENCES "home_about"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "home_about" ADD CONSTRAINT "fk_be0edb7328cdc89c" FOREIGN KEY ("home_id") REFERENCES "home"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "palmares" ADD CONSTRAINT "fk_ff4ee649fdff2e92" FOREIGN KEY ("thumbnail_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "social" ADD CONSTRAINT "fk_7161e18773f32dd8" FOREIGN KEY ("configuration_id") REFERENCES "configuration"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "sponsor" ADD CONSTRAINT "fk_818cc9d4fdff2e92" FOREIGN KEY ("thumbnail_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "tag_blog" ADD CONSTRAINT "fk_2e1aeef5bad26311" FOREIGN KEY ("tag_id") REFERENCES "tag"("uuid") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "tag_blog" ADD CONSTRAINT "fk_2e1aeef5dae07e97" FOREIGN KEY ("blog_id") REFERENCES "blog"("uuid") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "team" ADD CONSTRAINT "fk_c4e0a61ffdff2e92" FOREIGN KEY ("thumbnail_id") REFERENCES "file"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "account" ADD CONSTRAINT "account_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE;
