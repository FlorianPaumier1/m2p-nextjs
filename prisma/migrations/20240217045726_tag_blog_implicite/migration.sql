/*
  Warnings:

  - You are about to drop the column `configuration_id` on the `social` table. All the data in the column will be lost.
  - You are about to drop the `tag_blog` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "social" DROP CONSTRAINT "fk_7161e18773f32dd8";

-- DropForeignKey
ALTER TABLE "tag_blog" DROP CONSTRAINT "fk_2e1aeef5bad26311";

-- DropForeignKey
ALTER TABLE "tag_blog" DROP CONSTRAINT "fk_2e1aeef5dae07e97";

-- DropIndex
DROP INDEX "idx_7161e18773f32dd8";

-- AlterTable
ALTER TABLE "social" DROP COLUMN "configuration_id",
ADD COLUMN     "configurationUuid" TEXT;

-- DropTable
DROP TABLE "tag_blog";

-- CreateTable
CREATE TABLE "_blogTotag" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_blogTotag_AB_unique" ON "_blogTotag"("A", "B");

-- CreateIndex
CREATE INDEX "_blogTotag_B_index" ON "_blogTotag"("B");

-- AddForeignKey
ALTER TABLE "social" ADD CONSTRAINT "social_configurationUuid_fkey" FOREIGN KEY ("configurationUuid") REFERENCES "configuration"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_blogTotag" ADD CONSTRAINT "_blogTotag_A_fkey" FOREIGN KEY ("A") REFERENCES "blog"("uuid") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_blogTotag" ADD CONSTRAINT "_blogTotag_B_fkey" FOREIGN KEY ("B") REFERENCES "tag"("uuid") ON DELETE CASCADE ON UPDATE CASCADE;
