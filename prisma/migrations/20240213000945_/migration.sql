/*
  Warnings:

  - You are about to drop the column `type` on the `file` table. All the data in the column will be lost.
  - You are about to drop the `thumbnail` table. If the table is not empty, all the data it contains will be lost.

*/
-- AlterTable
ALTER TABLE "file" DROP COLUMN "type",
ADD COLUMN     "path" VARCHAR(255) NOT NULL DEFAULT '',
ALTER COLUMN "name" SET DEFAULT '',
ALTER COLUMN "is_processed" DROP NOT NULL,
ALTER COLUMN "alt" SET DEFAULT '';

-- DropTable
DROP TABLE "thumbnail";
