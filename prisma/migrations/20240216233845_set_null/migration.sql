-- DropForeignKey
ALTER TABLE "configuration" DROP CONSTRAINT "fk_7161e18773f32dd8";

-- AddForeignKey
ALTER TABLE "configuration" ADD CONSTRAINT "fk_7161e18773f32dd8" FOREIGN KEY ("logo_id") REFERENCES "file"("uuid") ON DELETE SET NULL ON UPDATE NO ACTION;
