-- CreateTable
CREATE TABLE "home_about" (
    "uuid" TEXT NOT NULL,
    "home_id" TEXT NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "content" TEXT NOT NULL,

    CONSTRAINT "home_about_pkey" PRIMARY KEY ("uuid")
);

-- AddForeignKey
ALTER TABLE "home_about" ADD CONSTRAINT "fk_5e3e3e3e28cdc89c" FOREIGN KEY ("home_id") REFERENCES "home"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;
