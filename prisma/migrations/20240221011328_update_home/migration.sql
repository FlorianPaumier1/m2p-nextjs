/*
  Warnings:

  - You are about to drop the column `about_id` on the `home` table. All the data in the column will be lost.
  - You are about to drop the `home_about` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "about_item" DROP CONSTRAINT "fk_c0e0e4ed087db59";

-- DropForeignKey
ALTER TABLE "home" DROP CONSTRAINT "fk_71d60cd0d087db59";

-- DropForeignKey
ALTER TABLE "home_about" DROP CONSTRAINT "fk_be0edb7328cdc89c";

-- DropIndex
DROP INDEX "uniq_71d60cd0d087db59";

-- AlterTable
ALTER TABLE "home" DROP COLUMN "about_id";

-- DropTable
DROP TABLE "home_about";

-- CreateTable
CREATE TABLE "hero_text_line" (
    "uuid" TEXT NOT NULL,
    "home_id" TEXT NOT NULL,
    "content" VARCHAR(255) NOT NULL,
    "sort" INTEGER NOT NULL,

    CONSTRAINT "hero_text_line_pkey" PRIMARY KEY ("uuid")
);

-- CreateIndex
CREATE INDEX "idx_5e3e3e3e28cdc89c" ON "hero_text_line"("home_id");

-- AddForeignKey
ALTER TABLE "hero_text_line" ADD CONSTRAINT "fk_5e3e3e3e28cdc89c" FOREIGN KEY ("home_id") REFERENCES "home"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION;
