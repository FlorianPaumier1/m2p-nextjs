'use client';

import * as React from 'react';
import {SponsorType} from "@/repository/Sponsor";
import {v4} from "uuid";
import {Suspense, useEffect, useRef, useState} from "react";
import {config} from "react-spring";
import dynamic from "next/dynamic";
import Image from "next/image";

type Props = {
    sponsors: SponsorType[]
};
const SponsorCaroussel = ({sponsors = []}: Props) => {

    const table = sponsors.map((element, index) => {
        return {key: v4(), content: <section className="relative h-48 w-48"><Image
                src={element.thumbnail.path}
                alt={`Logo of ${element.name}`}
                fill={true}
                className="rounded-t w-full h-full object-cover"
                sizes="(max-width: 768px) 10rem, (max-width: 1200px) 15rem, 20rem"
            /></section>, onClick: () => setGoToSlide(index)};
    });

    const [index, setIndex] = useState(0);
    const [offsetRadius, setOffsetRadius] = useState(15);
    const [showArrows, setShowArrows] = useState(false);
    const [goToSlide, setGoToSlide] = useState(null);
    const [cards] = useState(table);
    const autoPlayRef = useRef();

    const [Carousel, setCarousel] = useState(null)

    useEffect(() => {
        clearInterval(autoPlayRef.current);
        const loadCarousel = () => {
            const DynamicCarousel = dynamic(
                () => import('react-spring-3d-carousel')
            )
            setCarousel(() => DynamicCarousel)


            // @ts-ignore
            autoPlayRef.current = setInterval(() => {
                setIndex((index) => index + 1);
            }, 5000);
        }


        loadCarousel()

        return () => clearInterval(autoPlayRef.current);
    }, [])

    if (!Carousel) {
        return
    }


    return (
        <section className="bg-primary">
            <div className="w-1/2 mx-auto min-h-80 flex justify-center items-center relative">
                <Suspense>
                    <Carousel
                        slides={cards}
                        goToSlide={index}
                        offsetRadius={offsetRadius}
                        showNavigation={showArrows}
                        animationConfig={config.stiff}
                    />
                </Suspense>
            </div>
        </section>
    );
};

export default SponsorCaroussel
