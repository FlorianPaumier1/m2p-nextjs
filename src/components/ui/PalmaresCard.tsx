// @flow
import * as React from 'react';
import {Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle} from "@/components/ui/card";
import Image from "next/image";
import moment from "moment/moment";
import {PalamaresType} from "@/repository/Palmares";


const PalmaresCard = ({palmares}: {palmares: PalamaresType}) => {
    return (
        <Card className="w-[25rem]">
            <CardHeader className="relative h-60">
                <Image
                    src={palmares.thumbnail.path}
                    alt={palmares.thumbnail.alt}
                    fill={true}
                    className="rounded-t"
                    sizes="(max-width: 768px) 10rem, (max-width: 1200px) 15rem, 20rem"
                />
            </CardHeader>
            <CardContent className="py-4">
                <CardTitle>{palmares.name}</CardTitle>
                <CardDescription>{palmares.description}</CardDescription>
            </CardContent>
            <CardFooter>
                {moment(palmares.date).format('DD/MM/YYYY')}
            </CardFooter>
        </Card>
    );
};

export default PalmaresCard
