// @flow
import * as React from 'react';
import {BentoGrid, BentoGridItem, Skeleton} from "@/components/ui/bento-grid";
import {BlogType} from "@/repository/Blog";
import Image from "next/image";
import {db} from "@/repository/prisma";
import {H2} from "@/components/ui/typographie";
import Link from "next/link";
import {Button} from "@/components/ui/button";
import moment from "moment";

const BlogHome = async () => {

    const items = await db.blog.findMany({
        include: {
            thumbnail: true
        },
        orderBy: {
            created_at: 'desc'
        },
        take: 4
    })

    return (
        <div>
            <div className=" 2xl:container 2xl:mx-auto md:py-12 lg:px-20 md:px-6 py-9 px-4  sm:block flex justify-center items-center flex-col sm:w-full xs:w-96 w-auto mx-auto">
                <div className=" text-center">
                    <h2 className=" md:w-full w-10/12 mx-auto font-semibold lg:text-4xl text-3xl lg:leading-9 leading-7 text-gray-800">Les derniers articles</h2>
                </div>
                <div className=" mt-10 grid lg:grid-cols-4 sm:grid-cols-2 grid-cols-1 lg:gap-8 gap-6">
                    {items.map((item: BlogType, index: number) => (
                        <div className="w-full h-full relative" key={index}>
                            <section className="relative min-h-96">
                                <Image
                                    className="w-full"
                                    src={item.thumbnail.path}
                                    alt="Article Thumbnail"
                                    fill
                                />
                            </section>
                            <div className=" bg-gray-800 text-white lg:p-8 py-8 px-4 text-center">
                                <p className=" font-noraml text-sm leading-3 text-white">{moment(item.created_at).format("MMM Do YY")}</p>
                                <h3 className=" font-semibold lg:text-2xl text-xl lg:leading-8 leading-7 text-white mt-2 mx-auto w-full sm:w-64 lg:w-auto">
                                    {item.title}
                                </h3>
                                <Link href={`/blog/${item.slug}`}><Button
                                    className=" focus:outline-none focus:ring-2 focus:ring-offset-2
                                border-2 border-white text-white bg-none hover:bg-white hover:text-gray-800
                                duration-150 text-base font-medium leading-4 mt-6 lg:w-auto w-full py-3 px-4"
                                >
                                    Voir l'article
                                </Button>
                                </Link>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
};

export default BlogHome
