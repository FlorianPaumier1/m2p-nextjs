// @flow
import * as React from 'react';
import {TeamType} from "@/repository/Team";
import Image from "next/image";

type Props = {
    people: TeamType,
    className: string
};
const PeopleImage = ({people, className}: Props) => {
    return (
        <div className={className}>
            <Image src={people.thumbnail?.path ?? `/default-${people.gender ?? "man"}.png`}
                alt="image of someone"
                className="w-20 xl:w-32 2xl:w-48 rounded-full"
                width={200}
                height={200}
            />
            <div className="flex flex-col jusitfy-center items-center space-y-2">
                <p className="text-lg xl:text-xl font-medium xl:font-bold leading-none xl:leading-5 text-center text-gray-800">
                    {people.full_name}
                </p>
                <p className="text-sm leading-none text-center text-gray-600">
                    {people.position}
                </p>
            </div>
        </div>
    );
};

export default PeopleImage
