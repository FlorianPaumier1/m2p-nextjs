// @flow
import * as React from 'react';
import {TeamType} from "@/repository/Team";
import { v4 } from 'uuid';
import PeopleImage from "@/components/ui/team/PeopleImage";

type Props = {
    teams: TeamType[]
};
const PeopleContainer = ({teams}: Props) => {

    const teamRows = [];

    teams.forEach((team, index) => {
        if (index % 2 === 0) {
            teamRows.push([]);
        }

        teamRows[teamRows.length - 1].push(team);
    })

    const stylesCard = [
        "w-64 xl:w-72 py-10 px-14 mb-4 md:mb-0 sm:mr-6 xl:mr-6 2xl:mr-8 space-y-8 bg-white flex justify-center" +
        " items-center flex-col shadow hover:shadow-xl transition duration-500 ease-in-out border rounded-lg border-gray-100",
        "w-64 xl:w-72 py-10 sm:mt-14 xl:mt-32  space-y-8 bg-white flex justify-center items-center flex-col shadow hover:shadow-xl transition" +
        " duration-500 ease-in-out border rounded-lg border-gray-100"
    ]

    return (
        <div>
            <div className="py-12 px-0 md:px-14 xl:px-8 flex justify-center items-center flex-col">
                <div className="mt-8 xl:mt-14 px-12 md:px-10 2xl:px-14 flex relative justify-center items-center">
                    <div className="grid grid-cols-1 xl:grid-cols-2 justify-center items-center space-y-4 sm:space-y-6 xl:space-y-0 w-full xl:space-x-6 relative z-10">
                        {teamRows.map((teamRow, index) => {
                            return <div key={v4()} className="flex sm:justify-start items-center justify-center flex-col sm:flex-row sm:items-start">
                                {teamRow.map((team, index) => {
                                    return <PeopleImage people={team} key={v4()} className={stylesCard[index]} />
                                })}
                            </div>
                        })}
                    </div>
                </div>
            </div>
        </div>

    )
}

export default PeopleContainer
