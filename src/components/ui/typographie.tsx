import {ReactNode} from "react";
import {cn} from "@/lib/utils";

export function H1({children, center}: {children: ReactNode, center?: boolean}) {
    return (
        <h1 className={`scroll-m-20 text-3xl font-extrabold tracking-tight lg:text-5xl ${center ? 'text-center' : ''}`}>
            {children}
        </h1>
    )
}

export function H2({children, className}: {children: ReactNode, className?: string}) {
    return (
        <h2 className={cn("scroll-m-20 border-b pb-2 text-2xl font-semibold tracking-tight", className)}>
            {children}
        </h2>
    )
}

export function H3() {
    return (
        <h3 className="scroll-m-20 text-xl font-semibold tracking-tight">
            The Joke Tax
        </h3>
    )
}

export function P({children}) {
    return (
        <p className="leading-7 [&:not(:first-child)]:mt-6">
            {children}
        </p>
    )
}

export function BlockQuote(children: ReactNode) {
    return (
        <blockquote className="mt-6 border-l-2 pl-6 italic">
            {children}
        </blockquote>
    )
}
