// @flow
import * as React from 'react';
import {Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle} from "@/components/ui/card";
import Image from "next/image";
import moment from "moment/moment";
import {PalamaresType} from "@/repository/Palmares";
import {SponsorType} from "@/repository/Sponsor";


const SponsorsCard = ({sponsor}: {sponsor: SponsorType}) => {
    return (
        <Card className="w-[25rem]">
            <CardHeader className="relative h-42">
                <Image
                    src={sponsor.thumbnail.path}
                    alt={`Logo of ${sponsor.name}`}
                    fill={true}
                    className="rounded-t w-full h-full object-cover"
                    sizes="(max-width: 768px) 10rem, (max-width: 1200px) 15rem, 20rem"
                />
            </CardHeader>
            <CardContent className="py-4">
                <CardTitle className="text-sm">{sponsor.name}</CardTitle>
                <CardDescription>{sponsor.description}</CardDescription>
            </CardContent>
            <CardFooter>
                {moment(sponsor.date).format('DD/MM/YYYY')}
            </CardFooter>
        </Card>
    );
};

export default SponsorsCard
