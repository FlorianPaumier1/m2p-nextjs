import * as React from 'react';
import dompurify from 'dompurify';

type Props = {
    className: string,
    content: string,
    stripHTML: boolean,
};

const ParsedInnerHtml = ({className, content, stripHTML}: Props) => {
    dompurify.addHook('afterSanitizeElements', (node: any) => {
        return node?.toString().replace(/<[^/>][^>]*><\/[^>]+>/, '')
    })

    const cleanHtml = (dirty: string) => {
        if (dirty.length > 100) dirty = dirty.slice(0, 100) + "...";
        return dompurify.sanitize(dirty, {USE_PROFILES: {html: !stripHTML}})
    }

    return (
        <div
            className={className}
            dangerouslySetInnerHTML={{__html: cleanHtml(content)}}
        />
    )
}

export default ParsedInnerHtml
