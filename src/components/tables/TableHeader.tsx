// @flow
import * as React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronDown, faChevronRight, faChevronUp} from "@fortawesome/free-solid-svg-icons";

type Props = {
    headers: string[],
    fields: string[],
    sortItems: Function,
    sort: boolean[]
    sortItem: {field: string, direction: string}
};
const TableHeader = ({headers, fields, sort, sortItems, sortItem}: Props) => {


    function getCaret(header: string) {
        if (sortItem.field !== null && sortItem.field === header) {
            return sortItem.direction === "asc" ? faChevronDown : faChevronUp
        }
        return faChevronRight
    }

    return (
        <thead className="">
        <tr className="text-white">
            {headers.map((header: string, index: number) => (
                    <th
                        key={index + header}
                        scope="col"
                        data-sort={fields.indexOf(header) > -1 ? fields[index] : null}
                        className="px-5 py-3 border-b border-gray-200 text-left text-sm uppercase font-normal
							    sorted align-center text-center h-20 text-sm leading-none text-gray-600"
                    >
                        <section
                            className="flex flex-row justify-center px-6 align-center text-center items-center"
                        >
                            {header}
                            {["id", "Actions"].indexOf(header) === -1 && (sort.length == 0 || sort[index]) && (
                                <button
                                    onClick={() => sortItems(fields[index])}
                                    className="cursor-pointer ml-2 text-center items-center p-0"
                                >
                                    <FontAwesomeIcon icon={getCaret(header)} height={10} width={10} />
                                </button>
                            )}
                        </section>
                    </th>
                )
            )}
            <th className="px-5 py-3 border-b border-gray-200 text-left text-sm uppercase font-normal
							    sorted align-center text-center h-20 text-sm leading-none text-gray-600">Actions</th>
        </tr>
        </thead>
    );
};

export default TableHeader
