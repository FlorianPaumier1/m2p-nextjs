// @flow
import * as React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleLeft, faAngleRight, faChevronLeft, faChevronRight, faEllipsis} from "@fortawesome/free-solid-svg-icons";

export type IPagination = {
    currentPageNumber: number;
    idField: string;
    itemNumberPerPage: number;
    totalItemCount: number;
    pageCount: number;
    items: {
        [index: string]: any
    }[]
}

export function Pagination({pagination, changePage}: { pagination: IPagination, changePage: Function }) {

    let hideBefore = false;
    let hideAfter = false;

    if (pagination.totalItemCount === 0) return <></>

    return (<section className="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
            <div>
                <p className="text-sm text-gray-700">
                    de <span
                    className="font-medium">{pagination.currentPageNumber * pagination.itemNumberPerPage - (pagination.itemNumberPerPage - 1)}</span> à <span
                    className="font-medium">{pagination.currentPageNumber == pagination.pageCount ? pagination.totalItemCount : pagination.currentPageNumber * pagination.itemNumberPerPage}</span> sur <span
                    className="font-medium">{pagination.totalItemCount}</span> résultats </p>
            </div>
            <div>
                <nav className="pagination flex" aria-label="Pagination">
                    <button onClick={() => changePage(1)}
                        className={`${pagination.currentPageNumber == 1 || pagination.currentPageNumber - 1 == 1 ? "pointer-events-none" : ""}
                             flex items-center px-4 py-2  text-gray-700 transition-colors duration-200 transform bg-white rounded-l-md dark:bg-gray-800
                              dark:text-gray-200 hover:bg-indigo-600 dark:hover:bg-indigo-500 hover:text-white
                               dark:hover:text-gray-200 border border-r-0`}>
                        <span className="sr-only">Précédant</span>

                        <FontAwesomeIcon icon={faAngleLeft} width={25} height={25}/>
                    </button>
                    <button onClick={() => changePage(pagination.currentPageNumber - 1)}
                        className={`${pagination.currentPageNumber == 1 ? "pointer-events-none" : ""} 
                            flex items-center px-4 py-2 text-gray-700 transition-colors duration-200 transform
                            bg-white  dark:bg-gray-800 dark:text-gray-200 hover:bg-indigo-600
                            dark:hover:bg-indigo-500 hover:text-white dark:hover:text-gray-200 border`}> <span
                        className="sr-only">Précédant</span>
                        <FontAwesomeIcon icon={faChevronLeft} width={25} height={25}/>
                    </button>
                    {[...Array(pagination.pageCount)].map((page, index) => {
                        index = index + 1
                        if (0 < (pagination.currentPageNumber - 5) - index && !hideBefore) {
                            hideBefore = true
                            return <span key={index} className="pointer-events-none link">
                                <FontAwesomeIcon icon={faEllipsis} width={25} height={25}/>
                            </span>
                        }else if(0 > (pagination.currentPageNumber + 5) - index && !hideAfter){
                            hideAfter = true
                            return <span key={index} className="pointer-events-none link">
                                <FontAwesomeIcon icon={faEllipsis} width={25} height={25}/>
                            </span>
                        }else if (0 > (pagination.currentPageNumber + 5) - index && hideAfter){
                            return hideAfter
                        }else if (0 < (pagination.currentPageNumber - 5) - index && hideBefore){
                            return hideBefore
                        }
                        return <button key={index} onClick={() => changePage(index)}
                            className={`${pagination.currentPageNumber == index ? " link-active" : "link" }`}>
                            {index}
                        </button>
                    })}
                    <button
                        onClick={() => changePage(pagination.currentPageNumber + 1)} className={`${pagination.currentPageNumber == pagination.pageCount || pagination.totalItemCount == 0 ? "pointer-events-none" : ""} flex items-center px-4 py-2 text-gray-700 transition-colors duration-200 transform bg-white  dark:bg-gray-800 dark:text-gray-200 hover:bg-indigo-600 dark:hover:bg-indigo-500 hover:text-white dark:hover:text-gray-200 border`}  >
                        <span
                            className="sr-only">Suivant</span>
                        <FontAwesomeIcon icon={faChevronRight} width={25} height={25}/>
                    </button>
                    <button onClick={() => changePage(pagination.pageCount)} className={`${
                        pagination.currentPageNumber == pagination.pageCount
                        || pagination.currentPageNumber + 1 >= pagination.pageCount
                        || pagination.totalItemCount == 0 ? "pointer-events-none" : ""}
					   flex items-center px-4 py-2 text-gray-700 transition-colors duration-200 
					   bg-white rounded-r-md dark:bg-gray-800 dark:text-gray-200  border border-l-0
					    hover:bg-indigo-600 dark:hover:bg-indigo-500 hover:text-white dark:hover:text-gray-200`}> <span
                        className="sr-only">Suivant</span>
                        <span
                            className="w-4 h-4">
                            <FontAwesomeIcon icon={faAngleRight} height={25} width={25}/>
					</span>
                    </button>
                </nav>
            </div>
        </section>
    );
};
