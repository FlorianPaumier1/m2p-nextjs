// @flow
import * as React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faEye, faTrashCan} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import moment from "moment/moment";
import ParsedInnerHtml from "@/components/ui/ParsedInnerHtml";

type Props = {
    item: { [key: string]: any },
    fields: string[],
    actions: string[],
    url: string,
    deleteItem: Function,
    idField: string
};
const TableRow = ({item, actions, deleteItem, fields, url, idField}: Props) => {


    const dynamic_field = (item: { [key: string]: any }, field: string): any => {
        if (field.indexOf(".") > -1) {
            const fields = field.split(".")
            const index = fields.shift()!
            return dynamic_field(item[index], fields.join("."))
        }

        if (!item.hasOwnProperty(field)) throw new Error(`${field} don't exist`);
        if (item[field] === null) return ''

        switch (typeof item[field]) {
            case "boolean":
                return item[field] ? "Oui" : "Non";
            case "string":
                if (item[field].startsWith('$D') && item[field].endWith('Z')) return moment(item[field]).format("DD-MM-YYYY")
                return <ParsedInnerHtml className="" content={item[field]} stripHTML={false} />;
            case "object":

                let value;
                if (item[field].hasOwnProperty("timestamp")) {
                    value = moment.unix(item[field].timestamp).format("DD-MM-YYYY")
                } else if (Array.isArray(item[field])) {
                    value = item[field].map((i: any) => i.title ?? i.name).join(" | ")
                } else if (item[field] instanceof Date) {
                    value = moment(item[field]).format("DD-MM-YYYY")
                } else {
                    value = item[field].toString()
                }
                return value;
            default:
                return item[field];
        }
    }

    return (
        <tr key={item.uuid}>
            {fields.map((field, index) => (
                <td
                    key={`${item.uuid}-${field}`}
                    className="px-5 py-5 border-b border-gray-200 bg-white text-sm text-center"
                >{dynamic_field(item, field)}</td>
            ))}
            <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
               <span className="flex flex-row gap-2 justify-center">
                {actions.indexOf("show") > -1 && (
                    <Link
                        className=""
                        href={`${url}/${item[idField]}`}
                    >
                        <button
                            className="flex justify-center items-center p-0 w-10 h-10 bg-green-600 rounded-full
                            hover:bg-green-700 active:shadow-lg mouse shadow transition ease-in duration-200
                            focus:outline-none"
                        >
                            <FontAwesomeIcon
                                icon={faEye}
                                height={20}
                                width={20}
                                color="white"
                            />
                        </button>
                    </Link>
                )}
                   {actions.indexOf("edit") > -1 && (
                       <Link
                           href={`${url}/${item[idField]}/edit`}
                           className="flex justify-center items-center open-form fill-white p-0 w-10 h-10 bg-blue-600
                           rounded-full hover:bg-blue-700 active:shadow-lg mouse shadow transition ease-in duration-200
                           focus:outline-none"
                       >
                           <FontAwesomeIcon
                               icon={faEdit}
                               height={20}
                               width={20}
                               color="white"
                           />
                       </Link>
                   )}
                   {actions.indexOf("delete") > -1 && (
                       <button
                           onClick={() => deleteItem(item[idField])}
                           type="submit"
                           className="flex justify-center items-center fill-white p-0 w-10 h-10 bg-red-700 rounded-full
                            hover:bg-red-600 active:shadow-lg mouse shadow transition ease-in duration-200
                            focus:outline-none"
                       >
                           <FontAwesomeIcon
                               icon={faTrashCan}
                               height={20}
                               width={20}
                               color="white"
                           />
                       </button>
                   )}
               </span>
            </td>
        </tr>
    );
};

export default TableRow
