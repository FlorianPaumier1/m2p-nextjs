"use client"
import * as React from 'react';
import Link from "next/link";
import {useEffect, useState} from "react";
import moment from "moment";
import ParsedInnerHtml from "@/components/ui/ParsedInnerHtml";
import {IPagination, Pagination} from "@/components/tables/Pagination";
import {Repository} from "@/repository/prisma";
import {
    faChevronDown,
    faChevronRight,
    faChevronUp,
    faEdit,
    faEye,
    faSearch,
    faTrashCan
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {deleteItem, fetchData} from '@/repository/crud';
import TableHeader from "@/components/tables/TableHeader";
import TableRow from "@/components/tables/TableRow";
import {v4} from "uuid";

type Props = {
    headers: string[],
    fields: string[],
    actions: string[],
    sort: boolean[],
    url: string,
    itemName: string,
    model: string
};

let isLoading = false

const TableContainer = ({
                            headers,
                            fields,
                            actions,
                            sort,
                            itemName,
                            url,
                            model
                        }: Props) => {

    const [data, setData] = useState<IPagination>({
        currentPageNumber: 1,
        itemNumberPerPage: 20,
        totalItemCount: 0,
        pageCount: 0,
        idField: "id",
        items: []
    })
    const [search, setSearch] = useState('')
    const [currentPage, setCurrentPage] = useState(1)
    const [sortItem, setSort] = useState({field: 'uuid', direction: 'asc'})

    const changePage = async (pageNumber: number) => {
        setCurrentPage(pageNumber)
    }

    const handleDeleteItem = async (uuid: string) => {
        await deleteItem(model, uuid)
        setSort({field: 'uuid', direction: 'asc'})
    }

    const sortItems = (field: string) => {
        let direction = "asc"

        if (sortItem.field === field) {
            direction = sortItem.direction === "asc" ? "desc" : "asc"
        }

        setSort({field: field, direction: direction})
    }

    useEffect(() => {
        if (isLoading) return;

        isLoading = true
        fetchData(model, {
            currentPageNumber: data.currentPageNumber,
            itemNumberPerPage: data.itemNumberPerPage,
            search: search,
            sortItem: sortItem
        }).then((response: IPagination) => {
            setData({
                ...response
            })
            isLoading = false
        })
    }, [search, sortItem, currentPage])

    return (
        <div className="py-8 relative">
            <div className="flex flex-row mb-1 sm:mb-0 justify-between w-full">
                <div className="flex flex-col ">
                    <label
                        htmlFor="search"
                        className="text-gray-800 text-sm font-bold leading-tight tracking-normal mb-2"
                    >
                        Recherche
                    </label>
                    <div className="relative">
                        <div
                            className="absolute text-gray-600 flex
                            items-center pr-3 right-0 border-l h-full cursor-pointer"
                        >
                            <div className="pl-2 pr-1">
                                <FontAwesomeIcon icon={faSearch} width={10} height={10} />
                            </div>
                        </div>
                        <input
                            id="search"
                            onChange={(e) => setSearch(e.target.value)}
                            className="text-gray-600 dark:text-gray-400 bg-white font-normal pr-20 sm:pr-32 h-10
                                 flex items-center pl-2 text-sm border-gray-300 rounded border shadow"
                            placeholder="Recherche"
                        />
                    </div>
                </div>

                <div className="text-end">
                    {actions.indexOf("new") > -1 && (
                        <Link
                            href={`${url}/new`}
                            className="open-form flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-blue-600 rounded-lg
                                    shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500
                                    focus:ring-offset-2 focus:ring-offset-blue-200"
                        >
                            Nouveau {itemName}
                        </Link>
                    )}
                </div>
            </div>
            <div className="md:mx-auto px-4 sm:px-8 py-4 overflow-x-auto border rounded my-4 shadow">
                <div className="inline-block min-w-full overflow-hidden">
                    <table className="min-w-full leading-normal">
                        <TableHeader
                            headers={headers}
                            fields={fields}
                            sortItems={sortItems}
                            sort={sort}
                            sortItem={sortItem}
                        />
                        <tbody>
                        {data.items !== undefined && data?.items?.length > 0 && (
                            data.items.map((item: any, key: number) => (
                                <TableRow
                                    url={url}
                                    fields={fields}
                                    deleteItem={handleDeleteItem}
                                    actions={actions}
                                    item={item}
                                    key={v4()}
                                    idField={data.idField}
                                />
                            ))
                        )}
                        {(data.items === undefined || data?.items?.length === 0) && (
                            <tr>
                                <td colSpan={headers.length + 1} className="text-center">Pas de résultat</td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                    <Pagination pagination={data} changePage={changePage} />
                </div>
            </div>
        </div>
    );
};

export default TableContainer
