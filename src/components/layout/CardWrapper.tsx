// @flow
import * as React from 'react';
import {Card, CardContent, CardFooter, CardHeader, CardTitle} from '../ui/card';
import { Button } from '../ui/button';
import {ReactElement} from "react";

type Props = {
    header: string,
    content: ReactElement,
    footer?: ReactElement
};
const CardWrapper = ({header, content, footer}: Props) => {
    return (
        <Card className="w-[350px]">
            <CardHeader>
                <CardTitle>{header}</CardTitle>
            </CardHeader>
            <CardContent>
                {content}
            </CardContent>
            <CardFooter className="flex justify-between">
                {footer}
            </CardFooter>
        </Card>
    );
};

export default CardWrapper
