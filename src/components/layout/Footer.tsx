// @flow
import * as React from 'react';
import moment from "moment";

const Footer = () => {
    return (
        <div className="w-full bg-gray-800 py-12">
            <div className="container mx-auto xl:flex text-center xl:text-left lg:text-left">
                <div className="xl:w-3/6 sm:w-full lg:w-full text-center xl:text-left mb-6 xl:mb-0">
                    <p className="text-white sm:text-center lg:text-center xl:text-left">{moment().format('YYYY')} Florian Paumier. All Rights Reserved</p>
                </div>
                <div className="xl:w-3/6 sm:w-full">
                    <ul className="xl:flex lg:flex md:flex sm:flex justify-around">
                        <li className="text-white mb-3 xl:mb-0 lg:mb-0 md:mb-0 sm:mb-0">
                            <a href="/CGU.pdf">Conditions d'utilisation</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Footer
