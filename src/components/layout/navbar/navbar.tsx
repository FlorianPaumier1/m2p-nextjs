import React from "react";
import DesktopNav from "@/components/layout/navbar/DesktopNav";
import MobileNav from "@/components/layout/navbar/MobileNav";
import Menu from "@/repository/Menu";

export default async function Navbar() {

    const menuItems = await Menu.getMenuItems();

    return (
        <div className="h-full w-full relative">
            <DesktopNav items={menuItems} />
            <MobileNav items={menuItems} />
        </div>
    );
}
