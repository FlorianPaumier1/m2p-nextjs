"use client"
import * as React from 'react';
import {signOut} from "next-auth/react";

type Props = {

};
const LogOutButton = (props: Props) => {
    return (
        <button
            onClick={() => signOut()}
            className="cursor-pointer text-xs leading-3 text-gray-200"
        >
            Déconnexion
        </button>
    );
};

export default LogOutButton
