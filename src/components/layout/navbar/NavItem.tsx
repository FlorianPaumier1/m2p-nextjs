// @flow
import * as React from 'react';
import {menu} from "@prisma/client";
import Link from "next/link";
import {v4} from "uuid";

type Props = {
    item: menu
};
const NavItem = ({item}: Props) => {
    return (
        <li key={v4()} className="flex cursor-pointer text-black text-lg leading-3 tracking-normal
        mt-2 py-2 focus:text-indigo-700 focus:outline-none hover:scale-110">
            <Link href={item.path} className="flex items-center">
                {item.icon}
                <span className="ml-2 font-bold">{item.label}</span>
            </Link>
        </li>
    );
};

export default NavItem
