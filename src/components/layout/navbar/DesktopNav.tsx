// @flow
import * as React from 'react';
import NavItem from "@/components/layout/navbar/NavItem";
import {v4} from 'uuid';
import {menu} from '@prisma/client';
import Image from "next/image";

type Props = {
    items: menu[]
};

const DesktopNav = ({items}: Props) => {

    const leftSide = items.slice(0, 3);
    const rightSide = items.slice(3, 6);

    return (
        <nav className="lg:block hidden w-full">
            <div className="w-full h-32 relative">
                <Image src={"/bg-nav.jpeg"} alt="Image of a bicycle wheel" fill objectFit="cover" className="z-0 opacity-60"/>
                <div className="flex items-center justify-between absolute z-20 w-full">
                    <ul className="hidden xl:flex md:mr-6 xl:mr-16 justify-center gap-x-24 w-2/5">
                        {leftSide.map(item => <NavItem key={v4()} item={item} />)}
                    </ul>
                    <div className="flex w-1/5 sm:w-auto items-center sm:items-stretch justify-end sm:justify-start">
                        <div className="flex items-center gap-2 w-48 h-32">
                        </div>
                    </div>
                    <ul className="hidden xl:flex justify-center gap-x-24 w-2/5">
                        {rightSide.map(item => <NavItem key={v4()} item={item} />)}
                    </ul>
                </div>
            </div>
            <section className="absolute w-full h-48 top-0 flex justify-center z-10">
                <section className="relative w-48 h-48 bg-primary rounded-b-full bg-opacity-80">
                    <Image
                        loading="lazy"
                        alt="logo de l'association"
                        src="/media/logo.png"
                        fill
                        key={v4()}
                    />
                <span className="font-bold text-secondary visible lg:hidden">Association M2P</span>
                </section>
            </section>
        </nav>
    );
};

export default DesktopNav
