import * as React from 'react';
import Image from "next/image";
import {v4} from "uuid";
import LogOutButton from "@/components/layout/navbar/LogOutButton";
import {adminRoutes} from "@/lib/route";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTableColumns} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";


type Props = {};


const Sidebar = async ({...props}: Props) => {

    return (
        <nav
            id="Main" className="bg-gray-900 min-h-screen fixed flex justify-start items-start sm:w-72 flex-col left-0 top-0 bottom-0"
        >
            <button
                className="hidden xl:flex text-white hover:text-black focus:outline-none focus:text-black
            px-6 pt-6 items-center space-x-3 w-full justify-center"
            >
                <Image
                    src="/media/logo.png"
                    alt="logo plateforme"
                    className="hidden md:block"
                    width={100} key={v4()} height={100}
                />
            </button>
            <section
                id="sidebar"
                className="px-4 xl:mt-6 pb-20 flex flex-col w-full space-y-3 overflow-y-auto"
            >
                {adminRoutes.map(route => {
                    return <Link
                        key={v4()}
                        href={route.path} className="focus:outline-none flex jusitfy-start hover:text-white focus:bg-gray-400 focus:text-white
		   hover:bg-gray-400 text-gray-200 rounded py-3 pl-4 items-center space-x-6 w-full fill-white"
                    >
                        <FontAwesomeIcon
                            icon={route.svg}
                            height={25}
                            width={25}
                        />
                        <p className="text-lg lg:text-xl leading-4">{route.label}</p>
                    </Link>
                })}
            </section>
            <div className="absolute bg-gray-700 bottom-0 h-full py-4 px-3.5 w-full max-h-20">
                <section className="flex space-x-2 justify-around items-center ">
                    <div className="flex flex-col justify-start items-start space-y-2">
                        <LogOutButton />
                    </div>
                </section>
            </div>
        </nav>
    );
};

export default Sidebar
