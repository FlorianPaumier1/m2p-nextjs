"use client"
import * as React from 'react';
import 'react-quill/dist/quill.snow.css';
import dynamic from "next/dynamic";

const ReactQuill = dynamic(() => import("react-quill"), {
    ssr: false,
});

import {FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {UnprivilegedEditor} from "react-quill";

type EditorProps = {
    label: string,
    onChange: (value: string) => void,
    value?: string,
    className?: string
};


const quillModules = {
    toolbar: [
        [{ header: [1, 2, 3, false] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ list: 'ordered' }, { list: 'bullet' }],
        ['link', 'image'],
        [{ align: [] }],
        [{ color: [] }],
        ['code-block'],
        ['clean'],
    ],
};


const quillFormats = [
    'header',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'link',
    'image',
    'align',
    'color',
    'code-block',
];

// eslint-disable-next-line react/display-name
const FormTextAreaInput = ({onChange, label, value, className = "h-60"}: EditorProps) => {

    const handleChange = (value: string) => {
        onChange(value);
    }

    return (
        <FormItem className="py-4 min-h-96">
            <FormLabel
                className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed
                             peer-disabled:opacity-70"
            >
                {label}
            </FormLabel>
            <FormMessage />
            <ReactQuill
                className={className}
                theme="snow"
                onChange={handleChange}
                modules={quillModules}
                formats={quillFormats}
                defaultValue={value}
            />
        </FormItem>
    );
};

export default FormTextAreaInput
