import * as React from 'react';
import {FormControl, FormDescription, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {cn} from "@/lib/utils";
import {Button} from "@/components/ui/button";
import {CalendarIcon} from "lucide-react";
import {Popover, PopoverContent, PopoverTrigger} from '../ui/popover';
import moment from "moment";
import {ControllerRenderProps} from "react-hook-form";
import {Calendar} from "@/components/ui/calendar";
import {Input} from "@/components/ui/input";
import {ChangeEvent, ChangeEventHandler, ForwardedRef, forwardRef} from "react";
import { Switch } from '../ui/switch';

type Props = {
    label: string,
    checked: boolean,
    onChange: (value: boolean) => void
};

// eslint-disable-next-line react/display-name
const FormSlider = ({label, onChange, checked}: Props) => {

    return (
        <FormItem className="flex items-center gap-8 py-4">
            <FormLabel>{label}</FormLabel>
            <Switch
                defaultChecked={checked}
                onCheckedChange={onChange}
            />
        </FormItem>
    );
};

export default FormSlider
