'use client';

import * as React from 'react';
import {Input} from "@/components/ui/input";
import {
    ChangeEvent, Suspense,
    useEffect, useRef,
    useState
} from "react";
import {Button} from "@/components/ui/button";
import {uploadFile} from "@/lib/upload";
import {FormLabel} from "@/components/ui/form";
import {ControllerRenderProps} from "react-hook-form";
import {FileType} from "@/repository/File";
import {faFile, faXmark} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Image from "next/image";
import {getBase64} from "@/lib/utils";
import Link from "next/link";

type Props = {
    label: string,
    onChange: any,
    isLoading: boolean,
    name: string,
    image?: Partial<FileType>
};

const FormFileInput = (
    {label, onChange, isLoading, image}: Props
) => {

    const ref = useRef<HTMLInputElement>(null)
    const [file, setFile] = useState(image)
    const [error, setError] = useState(null)

    const onFileChange = async (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.files?.[0]) {
            const file = event.target.files[0]
            setFile({
                path: await getBase64(file)
            })
            const {data, success} = await uploadFile(file)
            if (success) {
                onChange(data.uuid)
                setFile(data)
            }
        }
    }

    const deleteFile = async () => {

        console.log(file)

        if (!file.uuid) {
            setFile(null)
            onChange("")
        }
        const req = await fetch(`/api/upload/${file.uuid}`, {
            method: "DELETE"
        })

        const res = await req.json()

        if (res.success) {
            onChange("")
            setFile(null)
        }
    }

    const pickFile = () => {
        ref.current?.click()
    }

    const type = file?.path.split('.').pop()


    if (file?.path.length > 0) {
        return (
            <Suspense fallback={"...Loading"}>
                <label>{label}</label>
            <div className="flex flex-col justify-center items-center">
                {type !== 'pdf' ? (
                    <div className="relative w-40 h-40">
                        <Image
                            src={error ? 'https://storage.googleapis.com/proudcity/mebanenc/uploads/2021/03/placeholder-image.png' : file.path}
                            alt="uploaded image"
                            className="object-contain"
                            fill
                            sizes="(max-width: 768px) 10rem, (max-width: 1200px) 15rem, 100%"
                            placeholder="empty"
                            onLoadingComplete={(result) => {
                                if (result.naturalWidth === 0) {
                                    // Broken image
                                    setError(result);
                                }
                            }}
                            onError={(e) => {
                                setError(e);
                            }}
                        />
                    </div>
                ) : (
                    <div className="relative flex items-center p-2 mt-2 rounded-md bg-background/10">
                        <FontAwesomeIcon icon={faFile} className="h-4 w-4" />
                        <Link
                            href={file.path}
                            target="_blank"
                            rel="noopener_noreferrer"
                            className="ml-2 text-sm text-indigo-500 dark:text-indigo-400 hover:underline"
                        >
                            View PDF
                        </Link>
                    </div>
                )}
                <Button
                    onClick={deleteFile}
                    variant="ghost"
                    type="button"
                >
                    <FontAwesomeIcon icon={faXmark} className="h-4 w-4" />
                    Supprimer
                </Button>
            </div>
            </Suspense>
        )
    }

    return (
        <>
            <FormLabel>
                {label}
            </FormLabel>
            <Button type="button" onClick={pickFile}>
                Choisissez un fichier
            </Button>
            <Input
                accept=".jpg, .jpeg, .png, .svg, .gif, .mp4"
                type="file"
                ref={ref}
                onChange={onFileChange}
                className="hidden"
            />
        </>
    );
}

export default FormFileInput
