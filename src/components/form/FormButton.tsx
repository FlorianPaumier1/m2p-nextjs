// @flow
import * as React from 'react';
import {Button} from "@/components/ui/button";

type Props = {
    content: string,
    type: "submit" | "button",
    action?: Function
};
const FormButton = ({type, content, action}: Props) => {

    const attr = {
        type: type,
        onClick: action ? () => action() : undefined
    }

    return (
        <Button
            variant="default"
            className="focus:ring-2 focus:ring-offset-2 focus:ring-indigo-700 text-sm font-semibold
                            leading-none text-white focus:outline-none bg-primary border rounded
                             py-4 w-full hover:bg-primary-light"
            {...attr}
        >
            {content}
        </Button>
    );
};

export default FormButton
