// @flow
import * as React from 'react';
import {FormControl, FormDescription, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Select, SelectContent, SelectItem, SelectTrigger, SelectValue} from "@/components/ui/select";
import {v4} from "uuid";

type Props = {
    field: any,
    label: string,
    placeholder: string,
    values: { value: string, label: string }[]
};
const FormSelect = ({values, placeholder, field, label}: Props) => {
    return (
        <FormItem>
            <FormLabel>{label}</FormLabel>
            <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl>
                    <SelectTrigger>
                        <SelectValue placeholder={placeholder} />
                    </SelectTrigger>
                </FormControl>
                <SelectContent>
                    {
                        values.map(
                            ({value, label}, index) => {
                                return <SelectItem key={v4()} value={value}>{label}</SelectItem>
                            }
                    )}
                </SelectContent>
            </Select>
            <FormMessage />
        </FormItem>
    );
};

export default FormSelect
