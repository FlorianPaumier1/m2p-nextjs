import * as React from 'react';
import {FormControl, FormDescription, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {cn} from "@/lib/utils";
import {Button} from "@/components/ui/button";
import {CalendarIcon} from "lucide-react";
import {Popover, PopoverContent, PopoverTrigger} from '../ui/popover';
import moment from "moment";
import {ControllerRenderProps} from "react-hook-form";
import {Calendar} from "@/components/ui/calendar";
import {Input} from "@/components/ui/input";
import {ChangeEvent, ChangeEventHandler, ForwardedRef, forwardRef} from "react";
import { Textarea } from '../ui/textarea';
import {Label} from "@/components/ui/label";

type Props = {
    label: string,
    value: string,
    onChange: (value: string) => void
};
// eslint-disable-next-line react/display-name
const FormTextAreaInput = ({label, onChange, value}: Props) => {

    return (
            <FormItem className="grid w-full gap-1.5 py-4">
                <FormLabel htmlFor="message">{label}</FormLabel>
                <Textarea
                    onChange={(e) => onChange(e.target.value)}
                    defaultValue={value}
                />
                <FormMessage />
            </FormItem>
    );
};

export default FormTextAreaInput
