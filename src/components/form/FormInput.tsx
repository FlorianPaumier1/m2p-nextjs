// @flow
import * as React from 'react';
import {FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Input} from "@/components/ui/input";
import {ControllerRenderProps} from "react-hook-form";
import {ForwardedRef, forwardRef} from "react";
import {Label} from "@/components/ui/label";
import {v4} from "uuid";

type Props = {
    description?: string,
    label: string,
    type: string,
    field: any,
};

// eslint-disable-next-line react/display-name
const FormInput = ({field, label, type, description}: Props) => {
    return (
        <FormItem>
            <FormLabel>{label}</FormLabel>
            <FormControl>
                <Input
                    type={type}
                    className="text-gray-600 focus:outline-none focus:ring-2
	            focus:ring-offset-2 focus:ring-indigo-700 bg-white font-normal w-full h-10
	          flex items-center pl-3 text-sm border-gray-300 rounded border shadow mb-4"
                    placeholder=""
                    {...field}
                    value={field.value ?? ""}
                />
            </FormControl>
            {description && (
                <FormDescription>
                    {description}
                </FormDescription>
            )}
            <FormMessage />
        </FormItem>
    );
};

export default FormInput
