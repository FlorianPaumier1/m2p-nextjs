// @flow
import * as React from 'react';
import {FormControl, FormDescription, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {cn} from "@/lib/utils";
import {Button} from "@/components/ui/button";
import {CalendarIcon} from "lucide-react";
import { Popover, PopoverContent, PopoverTrigger } from '../ui/popover';
import moment from "moment";
import {ControllerRenderProps} from "react-hook-form";
import {Calendar} from "@/components/ui/calendar";

type Props = {
    field: any,
    label: string,
    description: string
};
const FormDateInput = ({field, label, description}: Props) => {
    return (
        <FormItem className="flex flex-col">
            <FormLabel>{label}</FormLabel>
            <Popover>
                <PopoverTrigger asChild>
                    <FormControl>
                        <Button
                            variant={"outline"}
                            className={cn(
                                "pl-3 text-left font-normal",
                                !field.value && "text-muted-foreground"
                            )}
                        >
                            {field.value ? (
                                moment(field.value).format("DD-MM-YYYY")
                            ) : (
                                <span>Pick a date</span>
                            )}
                            <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                        </Button>
                    </FormControl>
                </PopoverTrigger>
                <PopoverContent className="w-auto p-0" align="start">
                    <Calendar
                        mode="single"
                        selected={field.value}
                        onSelect={field.onChange}
                        disabled={(date) =>
                            date < new Date("1900-01-01")
                        }
                        initialFocus
                        defaultMonth={field.value || new Date()}
                    />
                </PopoverContent>
            </Popover>
            <FormDescription>{description}</FormDescription>
            <FormMessage />
        </FormItem>
    );
};

export default FormDateInput
