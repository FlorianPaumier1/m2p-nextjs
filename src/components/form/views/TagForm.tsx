"use client"
import * as React from 'react';
import Link from "next/link";
import FormButton from "@/components/form/FormButton";
import {SocialSchema, TagSchema} from "@/lib/schema";
import FormInput from "@/components/form/FormInput";
import {z} from "zod";
import {useRouter} from "next/navigation";
import "react-toastify/dist/ReactToastify.css";
import {Form, FormField} from '@/components/ui/form';
import {useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {Suspense} from "react";
import {handleSocialSubmit, handleTagSubmit} from '@/lib/formSubmit';
import {toast} from "react-toastify";
import {SocialType} from "@/repository/Social";
import FormSelect from "@/components/form/FormSelect";
import {TagType} from "@/repository/Tag";

type Props = {
    item: TagType,
    title: string
};

const TagForm = ({item, title}: Props) => {
    const router = useRouter()

    const form = useForm<z.infer<typeof TagSchema>>({
        resolver: zodResolver(TagSchema),
        mode: "onChange",
        defaultValues: {
            name: item?.name
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await TagSchema.safeParseAsync(values)

        if (isValid.success) {

            const object: TagType = {
                uuid: item?.uuid,
                name: values?.name
            }

            const {data, success} = await handleTagSubmit(object)

            if (success) {
                router.push(`/admin/tags`)
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
            console.log(error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <Suspense>
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/tags"}
            >
                Retour
            </Link>
            <h1 className="text-4xl text-center">{title}</h1>
            <div className="w-full xl:w-2/3 items-center mx-auto">
                <Form {...form}>
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="name"
                        render={({field}) => (
                            <FormInput
                                label="Nom"
                                type="text"
                                field={field}
                                description="Nom du tag"
                            />
                        )}
                    />
                    <div className="w-1/5">
                        <FormButton type="submit" content="Valider" action={submit} />
                    </div>
                </Form>
            </div>
        </Suspense>
    )
        ;
};

export default TagForm
