"use client"
import * as React from 'react';
import Link from "next/link";
import Blog, {BlogType} from "@/repository/Blog";
import FormButton from "@/components/form/FormButton";
import {BlogSchema, TeamSchema} from "@/lib/schema";
import FormInput from "@/components/form/FormInput";
import FormFileInput from "@/components/form/FormFileInput";
import {v4} from "uuid";
import FormSlider from "@/components/form/FormSlider";
import FormTextAreaInput from "@/components/form/FormTextAreaInput";
import {z} from "zod";
import {useRouter} from "next/navigation";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from '@/components/ui/form';
import {ControllerRenderProps, useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {Input} from "@/components/ui/input";
import FormEditor from "@/components/form/FormEditor";
import {Suspense, useEffect, useState} from "react";
import {FileType} from "@/repository/File";
import {handleBlogSubmit, handleTeamSubmit} from '@/lib/formSubmit';
import {toast} from "react-toastify";
import {FormMultiSelect} from "@/components/form/FormMultiSelect";
import Tag from "@/repository/Tag";
import {findTagListSelect} from '@/repository/crud';
import {TeamType} from "@/repository/Team";
import FormSelect from "@/components/form/FormSelect";

type Props = {
    team: TeamType,
    title: string
};

const BlogForm = ({team, title}: Props) => {
    const router = useRouter()

    const form = useForm<z.infer<typeof TeamSchema>>({
        resolver: zodResolver(TeamSchema),
        mode: "onChange",
        defaultValues: {
            full_name: team?.full_name,
            position: team?.position,
            sort: team?.sort,
            thumbnail_id: team?.thumbnail_id,
            gender: team?.gender,
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await TeamSchema.safeParseAsync(values)

        if (isValid.success) {
            const req = await fetch('/api/admin/team', {
                method: 'POST',
                body: JSON.stringify({
                    uuid: team?.uuid,
                    full_name: values?.full_name,
                    position: values?.position,
                    sort: values?.sort,
                    thumbnail_id: values?.thumbnail_id,
                    gender: values?.gender,
                })
            })

            const {data, success} = await req.json()

            if (success) {
                router.push(`/admin/team/${data.uuid}`)
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <Suspense>
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/blog"}
            >
                Retour
            </Link>
            <h1 className="text-4xl text-center">{title}</h1>
            <div className="w-full xl:w-2/3 items-center mx-auto">
                <Form {...form}>
                    <section className="w-full lg:w-1/2">
                        <FormField
                            control={form.control}
                            name="thumbnail_id"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormItem className="grid w-full gap-1.5 py-4">
                                    <FormControl>
                                        <FormFileInput
                                            label={"Thumbnail"}
                                            onChange={field.onChange}
                                            isLoading={isLoading}
                                            image={team?.thumbnail}
                                            name="thumbnail_id"
                                        />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </section>
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="full_name"
                        render={({field}) => (
                            <FormInput
                                label="Nom Prénom"
                                type="text"
                                field={field}
                                description="Nom Prénom de la personne"
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="position"
                        render={({field}) => (
                            <FormInput
                                label="Titre"
                                type="text"
                                field={field}
                                description="Position de la personne"
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="sort"
                        render={({field}) => (
                            <FormInput
                                label="Ordre"
                                type="number"
                                field={field}
                                description="Ordre d'apparition"
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="gender"
                        render={({field}) => (
                            <span
                                className={"w-1/3"}
                            >
                            <FormSelect
                                label="Genre"
                                field={field}
                                placeholder="Choisissez un genre"
                                values={[
                                    {label: "Homme", value: "homme"},
                                    {label: "Femme", value: "femme"}
                                ]}
                            />
                            </span>
                        )}
                    />
                    <div className="w-1/5 mt-4">
                        <FormButton type="submit" content="Valider" action={submit} />
                    </div>
                </Form>
            </div>
        </Suspense>
    )
        ;
};

export default BlogForm
