"use client"
import * as React from 'react';
import {MenuType} from "@/repository/Settings";
import {Input} from "@/components/ui/input";
import {Switch} from "@/components/ui/switch";
import FormFileInput from "@/components/form/FormFileInput";
import {useForm} from "react-hook-form";
import {z} from "zod";
import {AboutSchema, BlogSchema, PalmaresSchema, SettingSchema} from "@/lib/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormField, FormItem, FormMessage} from "@/components/ui/form";
import FormInput from "@/components/form/FormInput";
import FormSlider from "@/components/form/FormSlider";
import FormButton from "@/components/form/FormButton";
import {toast} from "react-toastify";
import {useRouter} from "next/navigation";
import {AboutType} from "@/repository/About";
import FormEditor from "@/components/form/FormEditor";

type Props = {
    about: AboutType,
    handleSubmit: (values: Partial<AboutType>) => Promise<{data: any, success: boolean}>
};
const AboutForm = ({about, handleSubmit}: Props) => {

    const router = useRouter()

    const form = useForm<z.infer<typeof AboutSchema>>({
        resolver: zodResolver(AboutSchema),
        mode: "onChange",
        defaultValues: {
            content: about?.content,
            banner_id: about?.banner_id
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await AboutSchema.safeParseAsync(values)

        if (isValid.success) {

            const {data, success} = await handleSubmit({
                uuid: about?.uuid,
                content: values?.content,
                banner_id: values?.banner_id,
            })

            if (success) {
                router.refresh()
                toast.success("Les modifications ont été enregistrées")
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <div className="mx-auto">
            <Form {...form}>
                <section className="flex gap-4">
                    <section className="w-full lg:w-1/4 mx-auto my-4">
                        <FormField
                            control={form.control}
                            name="banner_id"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormItem className="grid w-full gap-1.5 py-4">
                                    <FormControl>
                                        <FormFileInput
                                            label={"Logo"}
                                            onChange={field.onChange}
                                            isLoading={isLoading}
                                            image={about?.banner}
                                            name="banner_id"
                                        />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </section>
                </section>
                <section className="w-full">
                    <FormField
                        control={form.control}
                        name="content"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormEditor
                                onChange={field.onChange}
                                label="Contenu à propos"
                                value={field.value}
                                className="h-[60vh] mb-8 pb-10"
                            />
                        )}
                    />
                </section>

                <div className="w-1/5">
                    <FormButton type="submit" content="Valider" action={submit} />
                </div>
            </Form>
        </div>
    );
};

export default AboutForm
