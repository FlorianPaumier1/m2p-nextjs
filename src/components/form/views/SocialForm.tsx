"use client"
import * as React from 'react';
import Link from "next/link";
import FormButton from "@/components/form/FormButton";
import {SocialSchema} from "@/lib/schema";
import FormInput from "@/components/form/FormInput";
import {z} from "zod";
import {useRouter} from "next/navigation";
import "react-toastify/dist/ReactToastify.css";
import {Form, FormField} from '@/components/ui/form';
import {useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {Suspense} from "react";
import {handleSocialSubmit} from '@/lib/formSubmit';
import {toast} from "react-toastify";
import {SocialType} from "@/repository/Social";
import FormSelect from "@/components/form/FormSelect";

type Props = {
    item: SocialType,
    title: string
};

const SocialForm = ({item, title}: Props) => {
    const router = useRouter()

    const form = useForm<z.infer<typeof SocialSchema>>({
        resolver: zodResolver(SocialSchema),
        mode: "onChange",
        defaultValues: {
            name: item?.name,
            url: item?.url,
            icon: item?.icon
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await SocialSchema.safeParseAsync(values)

        if (isValid.success) {

            const object: {[key: string]: any} = {
                uuid: item?.uuid,
                name: values?.name,
                url: values?.url,
                icon: values?.icon
            }

            const {data, success} = await handleSocialSubmit(object)

            if (success) {
                router.push(`/admin/social`)
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
            console.log(error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <Suspense>
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/scoial"}
            >
                Retour
            </Link>
            <h1 className="text-4xl text-center">{title}</h1>
            <div className="w-full xl:w-2/3 items-center mx-auto">
                <Form {...form}>
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="name"
                        render={({field}) => (
                            <FormInput
                                label="Nom"
                                type="text"
                                field={field}
                                description="Nom du palmares"
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="url"
                        render={({field}) => (
                            <FormInput
                                label="Url"
                                type={"text"}
                                field={field}
                                description="Url du réseau social"
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="icon"
                        render={({field}) => (
                            <div className="w-1/2 my-2">
                                <FormSelect
                                    field={field}
                                    label={"Réseau social"}
                                    placeholder={"Selectionnez un réseau social"}
                                    values={[
                                        {value: "facebook", label: "Facebook"},
                                        {value: "twitter", label: "Twitter"},
                                        {value: "instagram", label: "Instagram"},
                                        {value: "linkedin", label: "Linkedin"},
                                        {value: "youtube", label: "Youtube"},
                                        {value: "tiktok", label: "Tiktok"},
                                    ]}
                                />
                            </div>
                        )}
                    />
                    <div className="w-1/5">
                        <FormButton type="submit" content="Valider" action={submit} />
                    </div>
                </Form>
            </div>
        </Suspense>
    )
        ;
};

export default SocialForm
