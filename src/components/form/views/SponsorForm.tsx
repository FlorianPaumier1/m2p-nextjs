"use client"
import * as React from 'react';
import Link from "next/link";
import Blog, {BlogType} from "@/repository/Blog";
import FormButton from "@/components/form/FormButton";
import {BlogSchema, PalmaresSchema, SocialSchema, SponsorSchema} from "@/lib/schema";
import FormInput from "@/components/form/FormInput";
import FormFileInput from "@/components/form/FormFileInput";
import {v4} from "uuid";
import FormSlider from "@/components/form/FormSlider";
import FormTextAreaInput from "@/components/form/FormTextAreaInput";
import {z} from "zod";
import {useRouter} from "next/navigation";
import "react-toastify/dist/ReactToastify.css";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from '@/components/ui/form';
import {ControllerRenderProps, useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {Input} from "@/components/ui/input";
import FormEditor from "@/components/form/FormEditor";
import {Suspense, useEffect, useState} from "react";
import {FileType} from "@/repository/File";
import {handleBlogSubmit, handlePalmaresSubmit, handleSponsorSubmit} from '@/lib/formSubmit';
import {toast} from "react-toastify";
import { PalamaresType } from '@/repository/Palmares';
import FormDateInput from "@/components/form/FormDateInput";
import {SponsorType} from "@/repository/Sponsor";
import moment from "moment";

type Props = {
    item?: SponsorType,
    title: string
};

const SponsorForm = ({item, title}: Props) => {
    const router = useRouter()

    const form = useForm<z.infer<typeof SponsorSchema>>({
        resolver: zodResolver(SponsorSchema),
        mode: "onChange",
        defaultValues: {
            name: item?.name,
            date: item?.date,
            description: item?.description,
            isHighlighted: item?.isHighlighted,
            thumbnail_id: item?.thumbnail_id
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await SponsorSchema.safeParseAsync(values)

        if (isValid.success) {
            const {data, success} = await handleSponsorSubmit({
                uuid: item?.uuid,
                name: values?.name,
                date: values?.date,
                description: values?.description,
                isHighlighted: values?.isHighlighted,
                thumbnail_id: values?.thumbnail_id
            })

            if (success) {
                router.push(`/admin/sponsor/${data.uuid}`)
                return;
            }else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
            console.log(error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <Suspense fallback={"...Loading"}>
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/sponsor"}
            >
                Retour
            </Link>
            <h1 className="text-4xl text-center">{title}</h1>
            <div className="w-full xl:w-2/3 items-center mx-auto">
                <Form {...form}>
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="name"
                        render={({field}) => (
                            <FormInput
                                label="Nom"
                                type="text"
                                field={field}
                                description="Nom du palmares"
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="date"
                        render={({field}) => (
                            <FormDateInput
                                label="Date"
                                field={field}
                                description="Date du palmares"
                            />
                        )}
                    />
                    <section className="flex gap-4">
                        <section className="w-full lg:w-1/2">
                            <FormField
                                control={form.control}
                                name="thumbnail_id"
                                disabled={isLoading}
                                render={({field}) => (
                                    <FormItem className="grid w-full gap-1.5 py-4">
                                        <FormControl>
                                            <FormFileInput
                                                label={"Thumbnail"}
                                                onChange={field.onChange}
                                                isLoading={isLoading}
                                                image={item?.thumbnail}
                                                name="thumbnail_id"
                                            />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </section>
                    </section>
                    <FormField
                        control={form.control}
                        name="description"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormTextAreaInput
                                label="Description"
                                onChange={field.onChange}
                                value={field.value}
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="isHighlighted"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormSlider
                                label="Mettre en avant"
                                onChange={field.onChange}
                                checked={field.value}
                            />
                        )}
                    />
                    <div className="w-1/5">
                        <FormButton type="submit" content="Valider" action={submit} />
                    </div>
                </Form>
            </div>
        </Suspense>
    )
        ;
};

export default SponsorForm
