"use client"
import * as React from 'react';
import {HomeAboutType} from "@/repository/Home";
import {useForm} from "react-hook-form";
import {Form, FormControl, FormField, FormItem, FormMessage} from "@/components/ui/form";
import {z} from "zod";
import {HomeAboutSchema, HomeSchema} from "@/lib/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {useRouter} from "next/navigation";
import {H2} from '@/components/ui/typographie';
import FormInput from "@/components/form/FormInput";
import {Button} from "@/components/ui/button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {useState} from "react";
import FormButton from "@/components/form/FormButton";
import {toast} from "react-toastify";

type Props = {
    item: HomeAboutType[]
};
const HomeAboutForm = ({item}: Props) => {

    const router = useRouter()
    const [aboutItems, setAboutItems] = useState(item.map((item, index) => index) ?? [])

    const form = useForm<z.infer<typeof HomeAboutSchema>>({
        resolver: zodResolver(HomeAboutSchema),
        mode: "onSubmit",
        defaultValues: {
            items: item
        }
    })

    const isLoading = form.formState.isSubmitting

    const addElement = () => {
        setAboutItems([...aboutItems, aboutItems.length])
    }

    const removeElement = (index: number) => {
        setAboutItems(aboutItems.filter((item) => item !== index))
    }

    const submitHomeAbout = async () => {
        const values = form.getValues()

        values.items.forEach((item, index) => {
            item.sort = parseInt(item?.sort.toString())
        })

        const isValid: z.SafeParseReturnType<any, any> = await HomeAboutSchema.safeParseAsync({
            items: values.items.filter((item) => item)
        })

        if (isValid.success) {

            const req = await fetch("/api/admin/home_about", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(values.items)
            })

            const {data, success} = await req.json()

            if (success) {
                router.refresh()
                toast.success("Les modifications ont été enregistrées")
                return;
            } else {
                if (Array.isArray(data)) {
                    data.forEach(item => toast.error(item))
                } else {
                    toast.error(data)
                }
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>
        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
        })
    }

    return (
        <div>
            <H2>Elements à propos</H2>
            <Form {...form}>
                <section>
                    {aboutItems.map((index) => {
                        return <section
                            key={`text-${index}`}
                            className=" mt-4 [&:not(:last-child)]:border-b last:border-b-0 pb-4"
                        >
                            <FormField
                                control={form.control}
                                name={`items.${index}.uuid`}
                                disabled={isLoading}
                                render={({field}) => (
                                    <input {...field} type="hidden" />
                                )}
                            />
                                <FormField
                                    control={form.control}
                                    name={`items.${index}.title`}
                                    disabled={isLoading}
                                    render={({field}) => (
                                        <FormInput
                                            label={"Titre"}
                                            field={field}
                                            type="text"
                                        />
                                    )}
                                />

                                <FormField
                                    control={form.control}
                                    name={`items.${index}.content`}
                                    disabled={isLoading}
                                    render={({field}) => (
                                        <FormInput
                                            label={"Contenu"}
                                            field={field}
                                            type="text"
                                        />
                                    )}
                                />

                            <section className="w-1/4">
                                <FormField
                                    control={form.control}
                                    name={`items.${index}.sort`}
                                    disabled={isLoading}
                                    render={({field}) => (
                                        <FormInput
                                            label={"Ordre"}
                                            field={field}
                                            type="number"
                                        />
                                    )}
                                />
                            </section>
                            <Button onClick={() => removeElement(index)} className="mt-4">
                                <FontAwesomeIcon icon={faTrash} />
                            </Button>
                        </section>
                    })}
                </section>
                <Button onClick={addElement} className="my-4" type="button">
                    Ajouter un élément
                </Button>

                <div className="w-1/5">
                    <FormButton type="submit" content="Valider" action={submitHomeAbout} />
                </div>
            </Form>
        </div>
    );
};

export default HomeAboutForm
