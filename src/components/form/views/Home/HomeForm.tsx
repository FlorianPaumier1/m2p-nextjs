"use client"
import * as React from 'react';
import FormFileInput from "@/components/form/FormFileInput";
import {useForm} from "react-hook-form";
import {z} from "zod";
import {HomeSchema} from "@/lib/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormField, FormItem, FormMessage} from "@/components/ui/form";
import FormInput from "@/components/form/FormInput";
import FormSlider from "@/components/form/FormSlider";
import FormButton from "@/components/form/FormButton";
import {toast} from "react-toastify";
import {useRouter} from "next/navigation";
import { HomeType } from '@/repository/Home';
import { H2 } from '@/components/ui/typographie';
import {useEffect, useState} from "react";
import { v4 } from 'uuid';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {Button} from "@/components/ui/button";

type Props = {
    item: Partial<HomeType>
};
const SettingsForm = ({item}: Props) => {

    const router = useRouter()
    const [textBannerIndexes, setTextBannerCount] = useState(item?.hero_text.map((item, index) => index) ?? [])

    const form = useForm<z.infer<typeof HomeSchema>>({
        resolver: zodResolver(HomeSchema),
        mode: "onSubmit",
        defaultValues: {
            title: item?.title,
            caption: item?.caption,
            banner_id: item?.banner_id,
            home_text: item?.hero_text
        }
    })

    const addTextBanner = () => {
        setTextBannerCount([...textBannerIndexes, textBannerIndexes.length])
    }

    const removeTextBanner = (index: number) => {
        setTextBannerCount(textBannerIndexes.filter((item) => item !== index))
    }

    async function submitHome() {
        const values = form.getValues()

        values.home_text?.forEach((item, index) => {
            item.sort = parseInt(item?.sort.toString())
        })

        const isValid: z.SafeParseReturnType<any, any> = await HomeSchema.safeParseAsync(values)

        if (isValid.success) {

            const req = await fetch("/api/admin/home", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    uuid: item?.uuid,
                    title: values?.title,
                    caption: values?.caption,
                    banner_id: values?.banner_id,
                    home_text: values?.home_text
                })
            })

            const {data, success} = await req.json()

            console.log(data, success)

            if (success) {
                router.refresh()
                toast.success("Les modifications ont été enregistrées")
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <div className="w-full">
            <Form {...form}>
                <div>
                    <FormField
                        control={form.control}
                        name="title"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormInput
                                label={"Titre"}
                                field={field}
                                type="text"
                            />
                        )}
                    />
                </div>
                <div>
                    <FormField
                        control={form.control}
                        name="caption"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormInput
                                label={"Description"}
                                field={field}
                                type="text"
                            />
                        )}
                    />
                </div>
                <section className="flex gap-4">
                    <section className="w-full lg:w-1/2">
                        <FormField
                            control={form.control}
                            name="banner_id"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormItem className="grid w-full gap-1.5 py-4">
                                    <FormControl>
                                        <FormFileInput
                                            label={"Bannière"}
                                            onChange={field.onChange}
                                            isLoading={isLoading}
                                            image={item?.banner}
                                            name="banner_id"
                                        />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </section>
                </section>

                <div>
                    <H2>Bandeau de textes</H2>
                    {textBannerIndexes.map((index) => {
                        return <section key={`text-${index}`} className="flex gap-8 items-end mt-4">
                            <section className="w-full xl:w-1/2">
                                <FormField
                                    control={form.control}
                                    name={`home_text.${index}.uuid`}
                                    disabled={isLoading}
                                    render={({field}) => (
                                        <input {...field} type="hidden" />
                                    )}
                                />
                                <FormField
                                    control={form.control}
                                    name={`home_text.${index}.content`}
                                    disabled={isLoading}
                                    render={({field}) => (
                                        <FormInput
                                            label={"Contenu"}
                                            field={field}
                                            type="text"
                                        />
                                    )}
                                />
                            </section>
                            <section className="w-1/4">
                                <FormField
                                    control={form.control}
                                    name={`home_text.${index}.sort`}
                                    disabled={isLoading}
                                    render={({field}) => (
                                        <FormInput
                                            label={"Ordre"}
                                            field={field}
                                            type="number"
                                        />
                                    )}
                                />
                            </section>
                            <Button onClick={() => removeTextBanner(index)}>
                                <FontAwesomeIcon icon={faTrash} />
                            </Button>
                        </section>
                    })}
                    <Button onClick={addTextBanner} className="my-4">
                        Ajouter un élément
                    </Button>
                </div>

                <div className="w-1/5">
                    <FormButton type="submit" content="Valider" action={submitHome} />
                </div>
            </Form>
        </div>
    );
};

export default SettingsForm
