"use client";

import * as React from 'react';
import {z} from "zod";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {useForm} from "react-hook-form";
import {AboutSchema} from "@/lib/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import FormFileInput from "@/components/form/FormFileInput";
import FormInput from "@/components/form/FormInput";
import FormTextAreaInput from "@/components/form/FormTextAreaInput";
import {Input} from "@/components/ui/input";
import {Button} from '@/components/ui/button';

type Props = {};

const ContactForm = (props: Props) => {

    const schema = z.object({
        firstName: z.string(),
        lastName: z.string(),
        email: z.string().email(),
        subject: z.string(),
        message: z.string(),
    })

    const form = useForm<z.infer<typeof schema>>({
        resolver: zodResolver(schema),
        mode: "onSubmit"
    })

    const handleSubmit = (values: z.infer<typeof schema>) => {

    }

    const isLoading = form.formState.isSubmitting

    return (
        <Form {...form}>
            <form onSubmit={form.handleSubmit(handleSubmit)}>
                <section className="flex gap-8 mb-8">
                    <section className="w-1/2">
                        <FormField
                            control={form.control}
                            name="firstName"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormItem>
                                    <FormControl>
                                        <div className="relative h-11 w-full min-w-[200px]">
                                            <input
                                                placeholder="Prénom"
                                                onChange={field.onChange}
                                                value={field.value}
                                                className="peer h-full w-full border-b border-gray-200
                                                bg-transparent pt-4 pb-1.5 font-sans text-sm font-normal
                                                text-gray-700 outline outline-0 transition-all
                                                placeholder-shown:border-gray-200 focus:border-gray-500
                                                focus:outline-0
                                                placeholder:opacity-0 focus:placeholder:opacity-100"
                                            />
                                            <label
                                                className="after:content[''] pointer-events-none absolute left-0
                                                -top-1.5 flex h-full w-full select-none !overflow-visible truncate
                                                text-[11px] font-normal leading-tight text-gray-500 transition-all
                                                after:absolute after:-bottom-1.5 after:block after:w-full after:scale-x-0
                                                 after:border-b-2 after:border-gray-500 after:transition-transform
                                                 after:duration-300 peer-placeholder-shown:text-sm
                                                 peer-placeholder-shown:leading-[4.25]
                                                 peer-placeholder-shown:text-gray-500 peer-focus:text-[11px]
                                                 peer-focus:leading-tight peer-focus:text-gray-900
                                                 peer-focus:after:scale-x-100 peer-focus:after:border-gray-900"
                                            >
                                                Prénom
                                            </label>
                                        </div>
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </section>
                    <section className="w-1/2">
                        <FormField
                            control={form.control}
                            name="lastName"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormItem>
                                    <FormControl>
                                        <div className="relative h-11 w-full min-w-[200px]">
                                            <input
                                                placeholder="Nom"
                                                onChange={field.onChange}
                                                value={field.value}
                                                className="peer h-full w-full border-b border-gray-200 bg-transparent pt-4 pb-1.5 font-sans text-sm font-normal
                                                text-gray-700 outline outline-0 transition-all placeholder-shown:border-gray-200 focus:border-gray-500 focus:outline-0
                                                placeholder:opacity-0 focus:placeholder:opacity-100"
                                            />
                                            <label
                                                className="after:content[''] pointer-events-none absolute left-0
                                                -top-1.5 flex h-full w-full select-none !overflow-visible truncate
                                                text-[11px] font-normal leading-tight text-gray-500 transition-all
                                                after:absolute after:-bottom-1.5 after:block after:w-full after:scale-x-0
                                                 after:border-b-2 after:border-gray-500 after:transition-transform
                                                 after:duration-300 peer-placeholder-shown:text-sm
                                                 peer-placeholder-shown:leading-[4.25]
                                                 peer-placeholder-shown:text-gray-500 peer-focus:text-[11px]
                                                 peer-focus:leading-tight peer-focus:text-gray-900 peer-focus:after:scale-x-100
                                                 peer-focus:after:border-gray-900"
                                            >
                                                Nom
                                            </label>
                                        </div>
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </section>
                </section>
                <FormField
                    control={form.control}
                    name="email"
                    disabled={isLoading}
                    render={({field}) => (
                        <FormItem>
                            <FormControl>
                                <div className="relative h-12 w-full min-w-[200px] mb-8">
                                    <input
                                        placeholder="Email"
                                        onChange={field.onChange}
                                        value={field.value}
                                        className="peer h-full w-full border-b border-gray-200 bg-transparent pt-4 pb-1.5 font-sans text-sm font-normal
                                        text-gray-700 outline outline-0 transition-all placeholder-shown:border-gray-200 focus:border-gray-500 focus:outline-0
                                         placeholder:opacity-0 focus:placeholder:opacity-100"
                                    />
                                    <label
                                        className="after:content[''] pointer-events-none absolute left-0  -top-1.5 flex h-full w-full select-none !overflow-visible
                                        truncate text-[11px] font-normal leading-tight text-gray-500 transition-all after:absolute after:-bottom-1.5 after:block
                                        after:w-full after:scale-x-0 after:border-b-2 after:border-gray-500 after:transition-transform after:duration-300
                                        peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.25] peer-placeholder-shown:text-gray-500 peer-focus:text-[11px]
                                         peer-focus:leading-tight peer-focus:text-gray-900 peer-focus:after:scale-x-100 peer-focus:after:border-gray-900
                                         "
                                    >
                                        Email
                                    </label>
                                </div>
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="subject"
                    disabled={isLoading}
                    render={({field}) => (
                        <FormItem>
                            <FormControl>
                                <div className="relative h-11 w-full min-w-[200px] mb-8">
                                    <input
                                        placeholder="Suject"
                                        onChange={field.onChange}
                                        value={field.value}
                                        className="peer h-full w-full border-b border-gray-200 bg-transparent pt-4 pb-1.5 font-sans text-sm font-normal text-gray-700 outline outline-0 transition-all placeholder-shown:border-gray-200 focus:border-gray-500 focus:outline-0  placeholder:opacity-0 focus:placeholder:opacity-100"
                                    />
                                    <label
                                        className="after:content[''] pointer-events-none absolute left-0  -top-1.5 flex h-full w-full select-none !overflow-visible truncate text-[11px] font-normal leading-tight text-gray-500 transition-all after:absolute after:-bottom-1.5 after:block after:w-full after:scale-x-0 after:border-b-2 after:border-gray-500 after:transition-transform after:duration-300 peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.25] peer-placeholder-shown:text-gray-500 peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-gray-900 peer-focus:after:scale-x-100 peer-focus:after:border-gray-900 "
                                    >
                                        Sujet
                                    </label>
                                </div>
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="message"
                    disabled={isLoading}
                    render={({field}) => (
                        <FormItem>
                            <FormControl>
                                <div className="relative w-full min-w-[200px] mt-4 mb-8">
                                    <label
                                        htmlFor="message"
                                        className="block mb-2 text-sm font-medium text-gray-700 dark:text-gray-400"
                                    >Votre message</label>
                                    <textarea
                                        onChange={field.onChange}
                                        value={field.value}
                                        id="message"
                                        rows={10}
                                        className="block p-2.5 w-full text-sm text-gray-900 rounded-lg border border-gray-300"
                                        placeholder="Votre message..."
                                    ></textarea>
                                </div>
                            </FormControl>
                        </FormItem>
                    )}
                />

                <Button type={"submit"} className="w-full text-center py-6">Envoyer</Button>
            </form>
        </Form>
    );
};

export default ContactForm
