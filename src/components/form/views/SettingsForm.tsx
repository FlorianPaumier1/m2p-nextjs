"use client"
import * as React from 'react';
import {SettingsType} from "@/repository/Settings";
import {Input} from "@/components/ui/input";
import {Switch} from "@/components/ui/switch";
import FormFileInput from "@/components/form/FormFileInput";
import {useForm} from "react-hook-form";
import {z} from "zod";
import {BlogSchema, PalmaresSchema, SettingSchema} from "@/lib/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormField, FormItem, FormMessage} from "@/components/ui/form";
import FormInput from "@/components/form/FormInput";
import FormSlider from "@/components/form/FormSlider";
import FormButton from "@/components/form/FormButton";
import {toast} from "react-toastify";
import {useRouter} from "next/navigation";

type Props = {
    about: Partial<SettingsType>,
    handleSubmit: (values: Partial<SettingsType>) => Promise<{data: any, success: boolean}>
};
const SettingsForm = ({about, handleSubmit}: Props) => {

    const router = useRouter()

    const form = useForm<z.infer<typeof SettingSchema>>({
        resolver: zodResolver(SettingSchema),
        mode: "onChange",
        defaultValues: {
            title: about?.title,
            logo_id: about?.logo_id,
            maintenance: about?.maintenance || false,
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await SettingSchema.safeParseAsync(values)

        if (isValid.success) {

            const {data, success} = await handleSubmit({
                uuid: about?.uuid,
                title: values?.title,
                logo_id: values?.logo_id,
            })

            if (success) {
                router.refresh()
                toast.success("Les modifications ont été enregistrées")
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <div className="w-1/2 mx-auto">
            <Form {...form}>
                <div>
                    <FormField
                        control={form.control}
                        name="title"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormInput
                                label={"Titre"}
                                field={field}
                                type="text"
                            />
                        )}
                    />
                </div>
                <div>
                    <FormField
                        control={form.control}
                        name="maintenance"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormItem className="grid w-full gap-1.5 py-4">
                                <FormControl>
                                    <FormSlider
                                        label={"Mode Maintenance"}
                                        onChange={field.onChange}
                                        checked={field.value}
                                    />
                                </FormControl>
                                <FormMessage />
                            </FormItem>
                        )}
                    />
                </div>
                <section className="flex gap-4">
                    <section className="w-full lg:w-1/2">
                        <FormField
                            control={form.control}
                            name="logo_id"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormItem className="grid w-full gap-1.5 py-4">
                                    <FormControl>
                                        <FormFileInput
                                            label={"Logo"}
                                            onChange={field.onChange}
                                            isLoading={isLoading}
                                            image={about?.logo}
                                            name="logo_id"
                                        />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </section>
                </section>

                <div className="w-1/5">
                    <FormButton type="submit" content="Valider" action={submit} />
                </div>
            </Form>
        </div>
    );
};

export default SettingsForm
