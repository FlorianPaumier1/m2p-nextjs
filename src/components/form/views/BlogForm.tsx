"use client"
import * as React from 'react';
import Link from "next/link";
import Blog, {BlogType} from "@/repository/Blog";
import FormButton from "@/components/form/FormButton";
import {BlogSchema} from "@/lib/schema";
import FormInput from "@/components/form/FormInput";
import FormFileInput from "@/components/form/FormFileInput";
import {v4} from "uuid";
import FormSlider from "@/components/form/FormSlider";
import FormTextAreaInput from "@/components/form/FormTextAreaInput";
import {z} from "zod";
import {useRouter} from "next/navigation";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from '@/components/ui/form';
import {ControllerRenderProps, useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {Input} from "@/components/ui/input";
import FormEditor from "@/components/form/FormEditor";
import {Suspense, useEffect, useState} from "react";
import {FileType} from "@/repository/File";
import {handleBlogSubmit} from '@/lib/formSubmit';
import {toast} from "react-toastify";
import {FormMultiSelect} from "@/components/form/FormMultiSelect";
import Tag from "@/repository/Tag";
import {findTagListSelect} from '@/repository/crud';

type Props = {
    article: BlogType,
    title: string
};

const BlogForm = ({article, title}: Props) => {
    const router = useRouter()
    const [tags, setTags] = useState([])

    const form = useForm<z.infer<typeof BlogSchema>>({
        resolver: zodResolver(BlogSchema),
        mode: "onChange",
        defaultValues: {
            title: article?.title,
            caption: article?.caption,
            thumbnail_id: article?.thumbnail_id,
            banner_id: article?.banner_id,
            published: article?.published || false,
            content: article?.content,
            tags: article.tags?.map((tag) => tag.uuid) ?? []
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await BlogSchema.safeParseAsync(values)

        if (isValid.success) {
            const req = await fetch('/api/admin/blog', {
                method: 'POST',
                body: JSON.stringify({
                    uuid: article?.uuid,
                    title: values?.title,
                    caption: values?.caption,
                    thumbnail_id: values?.thumbnail_id,
                    banner_id: values?.banner_id,
                    published: values?.published,
                    content: values?.content,
                    tags: values?.tags
                })
            })

            const {success, data}: {success: boolean, data: any} = await req.json()

            if (success) {
                router.push(`/admin/blog/${data.uuid}`)
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
            console.log(error)
        })
    }

    const isLoading = form.formState.isSubmitting

    useEffect(() => {
        const fetchTags = async () => {
            const data = await findTagListSelect()
            setTags(data)
        }
        fetchTags()
    }, []);

    return (
        <Suspense>
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/blog"}
            >
                Retour
            </Link>
            <h1 className="text-4xl text-center">{title}</h1>
            <div className="w-full xl:w-2/3 items-center mx-auto">
                <Form {...form}>
                    <FormField
                        control={form.control}
                        disabled={isLoading}
                        name="title"
                        render={({field}) => (
                            <FormInput
                                label="Titre"
                                type="text"
                                field={field}
                                description="Titre de l'article"
                            />
                        )}
                    />
                    <section className="flex gap-4">
                        <section className="w-full lg:w-1/2">
                            <FormField
                                control={form.control}
                                name="thumbnail_id"
                                disabled={isLoading}
                                render={({field}) => (
                                    <FormItem className="grid w-full gap-1.5 py-4">
                                        <FormControl>
                                            <FormFileInput
                                                label={"Thumbnail"}
                                                onChange={field.onChange}
                                                isLoading={isLoading}
                                                image={article?.thumbnail}
                                                name="thumbnail_id"
                                            />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </section>
                        <section className="w-full lg:w-1/2">
                            <FormField
                                control={form.control}
                                name="banner_id"
                                disabled={isLoading}
                                render={({field}) => (
                                    <FormItem className="grid w-full gap-1.5 py-4">
                                        <FormControl>
                                            <FormFileInput
                                                label={"Bannière"}
                                                onChange={field.onChange}
                                                isLoading={isLoading}
                                                image={article?.banner}
                                                name="banner_id"
                                            />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </section>
                    </section>
                    <FormField
                        control={form.control}
                        name="published"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormSlider
                                label="Publier ?"
                                onChange={field.onChange}
                                checked={field.value}
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="caption"
                        disabled={isLoading}
                        render={({field}) => (
                            <FormTextAreaInput
                                label="Description"
                                onChange={field.onChange}
                                value={field.value}
                            />
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="content"
                        disabled={isLoading}
                        render={({field}) => (
                            <>
                                <FormEditor
                                    label="Contenu"
                                    onChange={field.onChange}
                                    value={field.value}
                                />
                            </>
                        )}
                    />
                    <div className="w-1/2 py-2">
                        <FormField
                            control={form.control}
                            name="tags"
                            disabled={isLoading}
                            render={({field}) => (
                                <>
                                    <FormItem>
                                        <FormLabel>Choissez des Tag</FormLabel>
                                        <FormMultiSelect
                                            onChange={field.onChange}
                                            options={tags}
                                            selected={field.value}
                                        />
                                        <FormMessage />
                                    </FormItem>
                                </>
                            )}
                        />
                    </div>
                    <div className="w-1/5">
                        <FormButton type="submit" content="Valider" action={submit} />
                    </div>
                </Form>
            </div>
        </Suspense>
    )
        ;
};

export default BlogForm
