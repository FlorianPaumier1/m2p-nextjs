"use client"
import * as React from 'react';
import {Input} from "@/components/ui/input";
import {Switch} from "@/components/ui/switch";
import FormFileInput from "@/components/form/FormFileInput";
import {useForm} from "react-hook-form";
import {z} from "zod";
import {BlogSchema, MenuSchema, PalmaresSchema, SettingSchema} from "@/lib/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormField, FormItem, FormMessage} from "@/components/ui/form";
import FormInput from "@/components/form/FormInput";
import FormSlider from "@/components/form/FormSlider";
import FormButton from "@/components/form/FormButton";
import {toast} from "react-toastify";
import {useRouter} from "next/navigation";
import {MenuType} from "@/repository/Menu";
import { handleMenuSubmit } from '@/lib/formSubmit';
import Link from "next/link";
import {H1} from "@/components/ui/typographie";
import { Suspense } from 'react';

type Props = {
    item: Partial<MenuType>
};
const SettingsForm = ({item}: Props) => {

    const router = useRouter()

    const form = useForm<z.infer<typeof MenuSchema>>({
        resolver: zodResolver(MenuSchema),
        mode: "onChange",
        defaultValues: {
            name: item?.name,
            label: item?.label,
            sort: item?.sort,
            enabled: item?.enabled || false,
        }
    })

    async function submit() {
        const values = form.getValues()

        const isValid: z.SafeParseReturnType<any, any> = await MenuSchema.safeParseAsync(values)

        if (isValid.success) {

            const {data, success} = await handleMenuSubmit({
                uuid: item?.uuid,
                name: values?.name,
                label: values?.label,
                sort: values?.sort,
                enabled: values?.enabled,
            })

            if (success) {
                router.push("/admin/menu")
                return;
            } else {
                toast.error("Une erreur est survenue")
            }
        }

        const isNotValid = isValid as z.SafeParseError<any>

        isNotValid.error.errors.forEach((error) => {
            // @ts-ignore
            form.setError(error.path[0], error)
        })
    }

    const isLoading = form.formState.isSubmitting

    return (
        <Suspense>
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/blog"}
            >
                Retour
            </Link>
            <div className="w-1/2 mx-auto py-8">
                <H1 center={true}>Editer le menu</H1>
                <Form {...form}>
                    <div className="mt-4">
                        <FormField
                            control={form.control}
                            name="name"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormInput
                                    label={"Nom"}
                                    field={field}
                                    type="text"
                                />
                            )}
                        />
                    </div>
                    <div>
                        <FormField
                            control={form.control}
                            name="label"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormInput
                                    label={"Label"}
                                    field={field}
                                    type="text"
                                />
                            )}
                        />
                    </div>
                    <div>
                        <FormField
                            control={form.control}
                            name="sort"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormInput
                                    label={"Ordre"}
                                    field={field}
                                    type="number"
                                />
                            )}
                        />
                    </div>
                    <div>
                        <FormField
                            control={form.control}
                            name="enabled"
                            disabled={isLoading}
                            render={({field}) => (
                                <FormItem className="grid w-full gap-1.5 py-4">
                                    <FormControl>
                                        <FormSlider
                                            label={"Activer"}
                                            onChange={field.onChange}
                                            checked={field.value}
                                        />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </div>
                    <div className="w-1/5">
                        <FormButton type="submit" content="Valider" action={submit} />
                    </div>
                </Form>
            </div>
        </Suspense>
    );
};

export default SettingsForm
