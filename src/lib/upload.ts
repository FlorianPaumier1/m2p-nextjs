export const uploadFile = async (value: File) => {
    const formData = new FormData()
    formData.set('file', value)

    const res = await fetch('/api/upload', {
        method: 'POST',
        body: formData
    })

    // handle the error
    if (!res.ok) throw new Error(await res.text())
    return await res.json()
}
