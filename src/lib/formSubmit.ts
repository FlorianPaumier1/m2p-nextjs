"use server"

import Blog, {BlogType} from "@/repository/Blog";
import Palmares, {PalamaresType} from "@/repository/Palmares";
import Sponsor, {SponsorType} from "@/repository/Sponsor";
import Social, {SocialType} from "@/repository/Social";
import Tag, {TagType} from "@/repository/Tag";
import Menu, {MenuType} from "@/repository/Menu";
import Team, {TeamType} from "@/repository/Team";

const handleBlogSubmit = async (value: Partial<BlogType>) => {
    return await Blog.persist(value)
}
const handlePalmaresSubmit = async (value: Partial<PalamaresType>) => {
    return await Palmares.persist(value)
}

const handleSponsorSubmit = async (value: Partial<SponsorType>) => {
    return await Sponsor.persist(value)
}

const handleSocialSubmit = async (value: SocialType) => {
    return await Social.persist(value)
}

const handleTagSubmit = async (value: TagType) => {
    return await Tag.persist(value)
}

const handleMenuSubmit = async (value: MenuType) => {
    return await Menu.persist(value)
}

const handleTeamSubmit = async (value: TeamType) => {
    return await Team.persist(value)
}

export {
    handleBlogSubmit, handlePalmaresSubmit, handleSponsorSubmit,
    handleSocialSubmit, handleTagSubmit, handleMenuSubmit,handleTeamSubmit
}
