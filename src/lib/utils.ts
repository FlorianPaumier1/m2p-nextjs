import {type ClassValue, clsx} from "clsx"
import {twMerge} from "tailwind-merge"
import {Repository} from "@/repository/prisma";
import {IPagination} from "@/components/tables/Pagination";
import {ControllerFieldState, ControllerRenderProps, UseFormStateReturn} from "react-hook-form";

export function cn(...inputs: ClassValue[]) {
    return twMerge(clsx(inputs))
}


export type formField = {
    name: string,
    label: string,
    type: string,
    required: boolean,
    placeholder: string
}

export type FormFieldProps = {
    field: ControllerRenderProps<any, string>,
    fieldState: ControllerFieldState,
    formState: UseFormStateReturn<any>
}
export function getBase64(file: File): Promise<any> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = (error) => reject(error);
    });
}

export const notDefaultInputType = ['file', 'hidden', 'date', 'textarea', 'slider', 'wysiwyg']

export function getLocalStorage(key: string, defaultValue:any){
    const stickyValue = localStorage.getItem(key);

    return (stickyValue !== null && stickyValue !== 'undefined')
        ? JSON.parse(stickyValue)
        : defaultValue;
}

export function setLocalStorage(key: string, value:any){
    localStorage.setItem(key, JSON.stringify(value));
}
