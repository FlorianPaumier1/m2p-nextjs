import {z} from "zod";

export const MAX_FILE_SIZE = 500000;
export const ACCEPTED_IMAGE_TYPES = ["image/jpeg", "image/jpg", "image/png", "image/webp"];


const LoginSchema = z.object({
    'email': z.string().email().min(1),
    'password': z.string().min(1),
})

const PalmaresSchema = z.object({
    name: z.string({
        required_error: "Le nom est obligatoire"
    }).min(1, {
        message: "Le nom est obligatoire"
    }),
    date: z.date(),
    thumbnail_id: z.string({
        required_error: "La thumbnail est obligatoire",
    }).min(1, {
        message: "La thumbnail est obligatoire"
    }),
    description: z.string({
        required_error: "La description est obligatoire"
    }).min(1, {
        message: "La description est obligatoire"
    }).max(255, {
        message: "La description est trop longue  (255 caractères maximum)"
    }),
})

const BlogSchema = z.object({
    uuid: z.string().optional().nullable(),
    thumbnail_id: z.string({
        required_error: "La thumbnail est obligatoire",
    }).min(1, {
        message: "La thumbnail est obligatoire"
    }),
    banner_id: z.string({
        required_error: "La bannière est obligatoire"
    }).min(1, {
        message: "La bannière est obligatoire"
    }),
    title: z.string({
        required_error: "Le titre est obligatoire"
    }).min(1, {
        message: "Le titre est obligatoire"
    }),
    content: z.string({
        required_error: "Le contenu est obligatoire"
    }).min(1, {
        message: "Le contenu est obligatoire"
    }),
    caption: z.string({
        required_error: "La description est obligatoire"
    }).min(1, {
        message: "La description est obligatoire"
    }).max(200, {
        message: "La description est trop longue  (200 caractères maximum)"
    }),
    published: z.boolean(),
    tags: z.array(z.string())
})

const AboutSchema = z.object({
    banner_id: z.string().min(1, {
        message: "La bannière est obligatoire"
    }),
    content: z.string().min(1, {
        message: "Le contenu est obligatoire"
    })
})

const MenuSchema = z.object({
    name: z.string(),
    label: z.string(),
    enabled: z.boolean(),
    sort: z.number().min(0),
})

const SocialSchema = z.object({
    url: z.string().min(1, {
        message: "L'url est obligatoire"
    }),
    icon: z.string(),
    name: z.string().min(1, {
        message: "Le nom est obligatoire"
    }),
})

const SponsorSchema = z.object({
    name: z.string({
        required_error: "Le nom est obligatoire"
    }).min(1, {
        message: "Le nom est obligatoire"
    }),
    date: z.date(),
    isHighlighted: z.boolean().default(false),
    thumbnail_id: z.string({
        required_error: "La thumbnail est obligatoire",
    }).min(1, {
        message: "La thumbnail est obligatoire"
    }),
    description: z.string({
        required_error: "La description est obligatoire"
    }).min(1, {
        message: "La description est obligatoire"
    }).max(255, {
        message: "La description est trop longue  (255 caractères maximum)"
    }),
})

const TagSchema = z.object({
    name: z.string().min(1, {
        message: "Le nom est obligatoire"
    })
})

const TeamSchema = z.object({
    thumbnail_id: z.string().optional(),
    full_name: z.string(),
    position: z.string(),
    gender: z.string().optional(),
    sort: z.number({
        required_error: "L'ordre est obligatoire",
        invalid_type_error: "L'ordre doit être un nombre"
    }),
})

const SettingSchema = z.object({
    title: z.string().min(1, {
        message: "Le titre est obligatoire"
    }).max(150, {
        message: "Le titre est trop long (150 caractères maximum)"
    }),
    logo_id: z.string({
        required_error: "Le logo est obligatoire"
    }).min(1, {
        message: "Le logo est obligatoire"
    }),
    maintenance: z.boolean(),
})

const HomeSchema = z.object({
    title: z.string().min(1, {
        message: "Le titre est obligatoire"
    }).max(150, {
        message: "Le titre est trop long (150 caractères maximum)"
    }),
    caption: z.string().min(1, {
        message: "La description est obligatoire"
    }).max(150, {
        message: "La description est trop long (150 caractères maximum)"
    }),
    banner_id: z.string({
        required_error: "La bannière est obligatoire"
    }).min(1, {
        message: "La bannière est obligatoire"
    }),
    home_text: z.array(z.object({
        uuid: z.string().optional().nullable(),
        content: z.string().default(""),
        sort: z.number().min(1).default(1)
    })).optional().nullable()
})

const HomeAboutSchema = z.object({
    items: z.array(z.object({
        uuid: z.string().optional().nullable(),
        title: z.string().min(1, {
            message: "Le titre est obligatoire"
        }).max(150, {
            message: "Le titre est trop long (150 caractères maximum)"
        }).default(""),
        content: z.string().min(1, {
            message: "Le contenu est obligatoire"
        }).default(""),
        sort: z.coerce.number().min(1).default(1)
    }))
})

export {
    LoginSchema, PalmaresSchema, BlogSchema, AboutSchema,
    MenuSchema, SocialSchema, SponsorSchema, TagSchema, TeamSchema,
    SettingSchema, HomeSchema, HomeAboutSchema
}
