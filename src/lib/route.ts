import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faCogs,
    faComment, faEllipsisVertical, faGlobe,
    faHouse,
    faMessage, faNetworkWired,
    faQuestion, faTableColumns, faTag,
    faTrophy, faUser,
    faUsers,
    IconDefinition
} from "@fortawesome/free-solid-svg-icons";

export const authRoute = [
    "/auth/login",
    "/auth/forgot-password",
];

export const apiAuthRoute = "/api/auth";



export const adminRoutes: {
    path: string,
    label: string,
    svg: IconDefinition
}[] = [
    {
        "path": "/admin/dashboard",
        "label": "Dashboard",
        "svg": faTableColumns
    },
    {
        "path": "/admin/blog",
        "label": "Blog",
        "svg": faMessage
    },
    {
        "path": "/admin/home",
        "label": "Accueil",
        "svg": faHouse
    },
    {
        "path": "/admin/about",
        "label": "A propos",
        "svg": faQuestion
    },
    {
        "path": "/admin/contact",
        "label": "Contact",
        "svg": faComment
    },
    {
        "path": "/admin/palmares",
        "label": "Palmares",
        "svg": faTrophy
    },
    {
        "path": "/admin/sponsor",
        "label": "Sponsor",
        "svg": faUsers
    },
    {
        "path": "/admin/tags",
        "label": "Tags",
        "svg": faTag
    },
    {
        "path": "/admin/menu",
        "label": "menus",
        "svg": faEllipsisVertical
    },
    {
        "path": "/admin/team",
        "label": "Equipe",
        "svg": faUser
    },
    {
        "path": "/admin/social",
        "label": "Réseaux Sociaux",
        "svg": faNetworkWired
    },
    {
        "path": "/admin/settings",
        "label": "Configuration",
        "svg": faCogs
    },
    {
        "path": "/admin/preview",
        "label": "Voir le site",
        "svg": faGlobe
    }
];
