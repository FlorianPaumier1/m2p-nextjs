//@ts-ignore
import {NextAuthConfig} from "next-auth";
import Credentials from "next-auth/providers/credentials";
import User from "@/repository/User";
import {LoginSchema} from "@/lib/schema";
import {compare} from "bcryptjs";

export default {
    providers: [
        Credentials({
            name: "credentials",
            async authorize(credentials, req) {
                const validateFields = LoginSchema.safeParse(credentials);

                if (!validateFields.success) return {};

                const { email, password } = validateFields.data;

                const user = await User.login(email)

                if (!user) return {};

                return await compare(password, user.password) ? user : null;
            }
        })
    ],
    secret: process.env.NEXTAUTH_SECRET,
} satisfies NextAuthConfig
