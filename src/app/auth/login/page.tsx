"use client"
import React from "react";
import {z} from "zod";
import {Form, FormControl, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {zodResolver} from "@hookform/resolvers/zod";
import {useForm} from "react-hook-form";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {signIn} from "next-auth/react";
import {LoginSchema} from "@/lib/schema";
import {redirect, useRouter} from "next/navigation";
import FormButton from "@/components/form/FormButton";

function MyApp() {

    const router = useRouter()

    const form = useForm<z.infer<typeof LoginSchema>>({
        resolver: zodResolver(LoginSchema),
        defaultValues: {
            email: "",
        },
    })

    const submit = async (values: z.infer<typeof LoginSchema>) => {

        const form = await signIn(
            "credentials",
            {
                ...values,
                redirect: false,
            }
        )

        if (form?.error && form?.error === "CredentialsSignin") {
            console.log("Error")
        }

        router.replace('/admin/dashboard')
    };

    return (
        <div className="h-screen bg-gradient-to-tl from-green-400 to-indigo-900 w-full py-16 px-4">
            <div className="h-full flex flex-col items-center justify-center">
                <div className="bg-white shadow rounded lg:w-1/3  md:w-1/2 w-full p-10 mt-16">
                    <h1
                        tabIndex={0} role="heading"
                        aria-label="Login to your account"
                        className="w-full text-2xl text-center font-extrabold leading-6 text-gray-800 mb-8"
                    >
                        Ce connecter à votre compte
                    </h1>
                    <Form {...form}>
                        <form onSubmit={form.handleSubmit(submit)}>
                            <FormField
                                control={form.control}
                                name="email"
                                render={({field}) => (
                                    <FormItem className="mb-4">
                                        <FormLabel>Username</FormLabel>
                                        <FormControl>
                                            <Input
                                                type="email"
                                                className="text-gray-600 dark:text-gray-400 focus:outline-none focus:ring-2
	        focus:ring-offset-2 focus:ring-indigo-700 dark:focus:border-indigo-700
	         dark:border-gray-700 dark:bg-gray-800 bg-white font-normal w-full h-10
	          flex items-center pl-3 text-sm border-gray-300 rounded border shadow"
                                                placeholder=""
                                                {...field}
                                            />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                            <FormField
                                control={form.control}
                                name="password"
                                render={({field}) => (
                                    <FormItem className="mb-4">
                                        <FormLabel>Mot de passe</FormLabel>
                                        <FormControl>
                                            <Input
                                                type="password"
                                                className="text-gray-600 focus:outline-none focus:ring-2
	            focus:ring-offset-2 focus:ring-indigo-700 bg-white font-normal w-full h-10
	          flex items-center pl-3 text-sm border-gray-300 rounded border shadow mb-4"
                                                placeholder="" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                            <FormButton content={"Ce connecter"} type={"submit"} />
                        </form>
                    </Form>
                </div>
            </div>
        </div>
    );
}

export default MyApp;
