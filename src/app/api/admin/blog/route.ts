import Blog from "@/repository/Blog";
import {NextRequest, NextResponse} from "next/server";

export const POST = async (req: NextRequest, res: NextResponse) => {
    const body = await req.json()
    let response = {success: false, data:null}
    let status = 200
    try {
        response = await Blog.persist(body)
    }catch (e) {
        status = 500
        response.data = e.message
    }


    return NextResponse.json(response, {status: status})
}
