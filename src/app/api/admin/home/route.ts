import {persistHome, persistTextBanner} from "@/repository/Home";
import Team from "@/repository/Team";
import {NextRequest, NextResponse} from "next/server";

export const POST = async (req: NextRequest, res: NextResponse) => {
    const body = await req.json()
    let response = {success: false, data:null}
    let status = 200

    try {
        const home = await persistHome({
            uuid: body.uuid,
            title: body.title,
            caption: body.caption,
            banner_id: body.banner_id
        })

        const hero_text = body.home_text?.map((text: any) => {
            return {
                uuid: text.uuid,
                content: text.content,
                sort: text.sort,
                home_id: home.uuid
            }
        }) ?? []

        if (hero_text.length > 0) await persistTextBanner(home.uuid, hero_text)
        response = {success: true, data: home}
    } catch (e) {
        console.log(e)
        response = {success: false, data: e}
    }

    return NextResponse.json(response, {status: status})
}
