import {getHome, persistHome, persistTextBanner} from "@/repository/Home";
import Team from "@/repository/Team";
import {NextRequest, NextResponse} from "next/server";
import {db} from "@/repository/prisma";

export const POST = async (req: NextRequest, res: NextResponse) => {
    const body = await req.json()
    let response = {success: false, data: null}
    let status = 200

    const home = await getHome()

    if (!home) {
        return NextResponse.json({success: false, data: "L'accueil n'existe pas"}, {status: 404})
    }

    body.forEach((item: any) => {
        item.home_id = home.uuid
    })

    const toCreate = body.filter((item: any) => !item.uuid)
    const toUpdate = body.filter((item: any) => item.uuid)

    try {

        await db.home_about.createMany({
            data: toCreate
        })

        for (const toUpdateElement of toUpdate) {
            await db.home_about.update({
                where: {uuid: toUpdateElement.uuid},
                data: {
                    title: toUpdateElement.title,
                    content: toUpdateElement.content,
                    sort: toUpdateElement.sort,
                }
            })
        }
        response = {success: true, data: null}
    } catch (e) {
        console.log(e)
        response = {success: false, data: e}
    }

    return NextResponse.json(response, {status: status})
}
