import {upload} from "@/repository/File";
import {NextRequest, NextResponse} from "next/server";
import {NextApiRequest, NextApiResponse} from "next";
import {db} from "@/repository/prisma";

export async function POST(req: Request, res: Response) {
    const data = await req.formData()

    const file: File | null = data.get('file') as unknown as File

    const uploaded = await upload(file)

    return NextResponse.json({success: true, data: uploaded })
}
