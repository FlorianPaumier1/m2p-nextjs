import {NextApiRequest, NextApiResponse} from "next";
import {db} from "@/repository/prisma";
import {NextResponse} from "next/server";
import * as fs from "fs";

export async function DELETE(
    req: Request,
    { params }: { params: { uuid: string } }
) {
    const uuid = params.uuid

    const file = await db.file.findUnique({
        where: {
            uuid: uuid as string
        }
    })

    if (!file) {
        return NextResponse.json({success: false, message: 'File not found'})
    }

    try{
        const deletedFile = await db.file.delete({
            where: {
                uuid: file.uuid
            }
        })

        if (deletedFile) {
            fs.unlink(file.path, (err) => {
                if (err) {
                    return NextResponse.json({success: false, message: err.message})
                }
            })
        }
    }catch (e) {
        return NextResponse.json({success: false, message: e.message})
    }
    return NextResponse.json({success: true})
}
