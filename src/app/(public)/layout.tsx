import Navbar from "@/components/layout/navbar/navbar";
import Footer from "@/components/layout/Footer";
import GoogleAnalytics from "@/components/GoogleAnalytics";
import CookieBanner from "@/components/ui/CookieBanner";

export default function RootLayout({
                                       children,
                                   }: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <section className="min-h-screen">
            <GoogleAnalytics GA_MEASUREMENT_ID={"G-RBKN5HJ0R1"} />
            <section className="h-32">
            <Navbar />
            </section>
            {children}
            <Footer />
            <CookieBanner />
        </section>
    );
}
