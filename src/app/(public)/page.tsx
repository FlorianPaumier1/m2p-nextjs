// @flow
import * as React from 'react';
import {db} from "@/repository/prisma";
import {hash} from "bcryptjs";
import {H1, P} from "@/components/ui/typographie";
import Image from "next/image";
import SponsorCaroussel from "@/components/ui/SponsorCaroussel";
import BlogHome from "@/components/ui/blog/BlogHome";
import { Button } from '@/components/ui/button';
import Link from "next/link";

type Props = {};
const Page = async (props: Props) => {

    const home = await db.home.findFirst({
        include: {
            banner: true
        }
    });

    const sponsors = await db.sponsor.findMany({
        where: {
            isHighlighted: true
        },
        include: {
            thumbnail: true
        }
    })

    return (
        <>
            <section className="bg-hero h-[65vh] ">
                <section className="w-2/3 mx-auto flex justify-center items-center h-full gap-12">
                    <section className="h-full flex flex-col justify-center gap-12">
                        <h1 className="text-center text-5xl md:text-left lg:text-6xl">{home?.title}</h1>
                        <p className="text-xl text-center md:text-left">
                            {home?.caption}
                        </p>
                        <section className="flex gap-12">
                            <Link href={"/about"}><Button className="py-6 px-8 text-lg">Me découvrir</Button></Link>
                            <Link href={"/contact"}><Button className="py-6 px-8 text-lg">Me Contacter</Button></Link>
                        </section>
                    </section>
                    <section className="relative aspect-[3/4] h-full flex justify-center items-center">
                        <Image src={home?.banner.path} alt="" height={600} width={600} className="rounded-xl"/>
                    </section>
                </section>
            </section>
            <SponsorCaroussel sponsors={sponsors} />
            <BlogHome />
        </>
    );
};

export default Page
