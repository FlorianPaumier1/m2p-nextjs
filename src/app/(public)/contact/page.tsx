// @flow
import * as React from 'react';
import { Suspense } from 'react';
import Image from 'next/image';
import ContactForm from "@/components/form/views/ContactForm";

type Props = {

};
const Page = (props: Props) => {
    return (
        <div className="bg-hero flex justify-center items-center h-[85vh]">
            <section className="flex w-2/3 h-[45rem] mx-auto shadow-lg border rounded border-primary">
                <section className="w-1/3 h-full relative">
                    <Image src={"/contact-img.jpeg"} alt={"Marion Paumier sur un podium"} fill />
                </section>
                <section className="w-2/3 px-8 bg-white">
                    <h1 className="text-4xl my-12 underline">Me Contacter</h1>
                    <Suspense>
                        <ContactForm />
                    </Suspense>
                </section>
            </section>
        </div>
    );
};

export default Page
