// @flow
import * as React from 'react';
import {db} from "@/repository/prisma";
import PeopleContainer from "@/components/ui/team/PeopleContainer";
import {H1, H2} from "@/components/ui/typographie";
import Image from "next/image";

type Props = {};
const Page = async (props: Props) => {

    const teams = await db.team.findMany({
        include: {
            thumbnail: true
        }
    })

    const about = await db.about.findFirst({
        include: {
            banner: true
        }
    })

    return (
        <section className="mt-12">
            <section className="px-12 border-b">
                <H1 center>A proppos de moi</H1>
                <section className="w-1/2 h-auto min-h-20 mx-auto relative aspect-[16/9] my-8">
                    <Image src={about.banner.path} alt="Bannière à propos" fill className="h-auto" />
                </section>
                <div className="w-1/2 mx-auto my-12 text-2xl" dangerouslySetInnerHTML={{__html: about.content}}></div>
            </section>
            <section>
            <H2 className={"w-full text-center border-0"}>L'équipe composant l'association</H2>
                <PeopleContainer teams={teams} />
            </section>
        </section>
    );
};

export default Page
