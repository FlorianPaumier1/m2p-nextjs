// @flow
import {db} from '@/repository/prisma';
import * as React from 'react';
import Image from "next/image";
import {H1} from '@/components/ui/typographie';
import {Button} from "@/components/ui/button";
import Link from "next/link";

type Props = {
    params: {
        slug: string
    }
};
const Page = async ({params: {slug}}: Props) => {

    const blog = await db.blog.findUnique({
        where: {
            slug: slug
        },
        include: {
            banner: true
        }
    })

    return (
        <div>
            <section className="w-full h-[50vh] relative">
                <Image src={blog.banner.path} alt="Blog Article" fill />
            </section>
            <section className="w-1/2 mx-auto">
                <H1 center>{blog.title}</H1>
                <section className="" dangerouslySetInnerHTML={{__html: blog.content}}></section>
                <Link href={"/blog"}>Retour</Link>
            </section>
        </div>
    );
};

export default Page
