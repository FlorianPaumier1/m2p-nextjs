// @flow
import * as React from 'react';
import {H1} from "@/components/ui/typographie";
import {findHomeAbout, getHome} from "@/repository/Home";
import {Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import HomeForm from '@/components/form/views/Home/HomeForm';
import HomeAboutForm from '@/components/form/views/Home/HomeAboutForm';

const Page = async () => {

    const home = await getHome()
    const homeAbout = await findHomeAbout(home?.uuid)

    return (
        <div>
            <H1 center={true}>
                Page d'accueil
            </H1>
            <Tabs defaultValue="firstSection" className="w-2/3 my-6 mx-auto">
                <TabsList className="grid w-full grid-cols-2">
                    <TabsTrigger value="firstSection">Page d'accueil</TabsTrigger>
                    <TabsTrigger value="secondSection">A propos</TabsTrigger>
                </TabsList>
                <section className="border rounded shadow p-8 my-8">
                <TabsContent value="firstSection">
                    <HomeForm item={home}/>
                </TabsContent>
                <TabsContent value="secondSection">
                    <HomeAboutForm item={homeAbout}/>
                </TabsContent>
                </section>
            </Tabs>
        </div>
    );
};

export default Page
