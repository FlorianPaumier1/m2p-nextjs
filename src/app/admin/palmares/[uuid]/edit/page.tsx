import * as React from 'react';
import PalmaresForm from "@/components/form/views/PalmaresForm";
import Palmares from "@/repository/Palmares";

type Props = {
    params: { uuid: string }
};


const Page = async ({params}: Props) => {

    const article = await Palmares.find(params.uuid)

    return <PalmaresForm item={article} title="Editer le palmares" />
};

export default Page
