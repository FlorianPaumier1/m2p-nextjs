import * as React from 'react';
import PalmaresForm from "@/components/form/views/PalmaresForm";

type Props = {

};


const Page = async () => {

    return <PalmaresForm item={{
        uuid: null,
        name: "",
        date: new Date(),
        description: "",
        thumbnail_id: "",
        thumbnail: {
            path: ""
        }
    }} title="Nouveau Palamares" />
};

export default Page
