import * as React from 'react';
import BlogForm from "@/components/form/views/BlogForm";
import Blog from "@/repository/Blog";
import Menu from '@/repository/Menu';
import MenuForm from "@/components/form/views/MenuForm";

type Props = {
    params: { uuid: string }
};


const Page = async ({params}: Props) => {

    const menu = await Menu.find(params.uuid) ?? {}

    return <MenuForm item={menu}/>
};

export default Page
