"use client"
import * as React from 'react';
import TableContainer from "@/components/tables/TableContainer";
import {v4} from "uuid";
import Palmares from "@/repository/Palmares";
import CardWrapper from "@/components/layout/CardWrapper";


const Page = () => {

    return (
        <section className="w-full h-full">
            <TableContainer
                actions={['search', 'edit']}
                headers={['Nom', 'Label', 'Lien', 'Activer', 'Ordre']}
                fields={['name', 'label', 'path', 'enabled', 'sort']}
                sort={[true, true, false, false, true]}
                itemName="Menu"
                url="/admin/menu"
                model={"menu"}
                key={v4()}
            />
        </section>
    );
};

export default Page
