import * as React from 'react';
import TableContainer from "@/components/tables/TableContainer";
import {v4} from "uuid";
import Palmares from "@/repository/Palmares";
import CardWrapper from "@/components/layout/CardWrapper";
import {H1} from "@/components/ui/typographie";


const Page = () => {

    return (
        <section className="w-full h-full">
            <H1 center={true}>Listes des Contacts</H1>
            <TableContainer
                actions={['search', 'show']}
                headers={['Nom', 'Email', 'Sujet']}
                fields={['name', 'email', 'subject']}
                sort={[true, true, true]}
                itemName="Contact"
                url="/admin/contact"
                model={"contact"}
                key={v4()}
            />
        </section>
    );
};

export default Page
