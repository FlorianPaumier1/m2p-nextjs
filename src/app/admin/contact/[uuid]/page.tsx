import * as React from 'react';
import {H1, P} from "@/components/ui/typographie";
import {Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle} from "@/components/ui/card";
import Contact from "@/repository/Contact";
import {redirect} from "next/navigation";
import moment from "moment";
import { Button } from '@/components/ui/button';
import Link from "next/link";

type Props = {
    params: { uuid: string }
};
const Page = async ({params: { uuid }}: Props) => {


    const email = await Contact.find(uuid)


    if (!email) redirect("/admin/contact")
    moment.locale('fr');

    return (
        <div>
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/contact"}
            >
                Retour
            </Link>
            <H1 center={true}>Visualisation de l'email</H1>
            {email && (
                <Card className="my-4 w-1/2 mx-auto">
                    <CardHeader>
                        <CardTitle>
                            <ul className="list-none">
                                <li className="py-2">Nom du contact : {email?.name}</li>
                                <li className="py-2">Sujet: {email.subject}</li>
                            </ul>
                        </CardTitle>
                        <CardDescription>
                            Contacter le {moment(email.created_at).format("DD MMMM YYYY")}
                        </CardDescription>
                    </CardHeader>
                    <CardContent>
                        <P>
                            {email.message}
                        </P>
                    </CardContent>
                    <CardFooter>
                        <Link
                            type="button"
                            href={`/admin/contact/${uuid}/delete`}
                            className="bg-red-500 rounded px-4 py-2 text-white"
                        >
                            Supprimer
                        </Link>
                    </CardFooter>
                </Card>
            )}
        </div>
    );
};

export default Page
