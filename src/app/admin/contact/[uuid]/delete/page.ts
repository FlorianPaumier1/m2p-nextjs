// @flow
import {redirect} from 'next/navigation';
import * as React from 'react';
import Contact from "@/repository/Contact";

type Props = {
    params: { uuid: string }
};
const Page = ({params: {uuid}}: Props) => {
    Contact.delete(uuid)
    redirect('/admin/contact')
};

export default Page
