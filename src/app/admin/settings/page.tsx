// @flow
import * as React from 'react';
import {H1} from "@/components/ui/typographie";
import {db} from "@/repository/prisma";
import Settings, {SettingsType} from "@/repository/Settings";
import {Input} from "@/components/ui/input";
import {Switch} from "@/components/ui/switch";
import FormFileInput from "@/components/form/FormFileInput";
import SettingsForm from "@/components/form/views/SettingsForm";

type Props = {

};
const Page = async (props: Props) => {

    const config = await Settings.findFirst()

    const handleSubmit = async (values: Partial<SettingsType>) => {
        "use server"
        return Settings.persist(values)
    }

    return (
        <div>
            <H1 center={true}>
                Configuration du site
            </H1>
            <SettingsForm about={config} handleSubmit={handleSubmit}/>
        </div>
    );
};

export default Page
