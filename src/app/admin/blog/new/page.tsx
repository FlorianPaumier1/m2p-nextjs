import * as React from 'react';
import BlogForm from "@/components/form/views/BlogForm";

type Props = {

};


const Page = async () => {

    return <BlogForm article={{
        thumbnail: {
            uuid: "",
            path: ""
        },
        banner: {
            uuid: "",
            path: ""
        },
        title: '',
        content: '',
        caption: '',
        published: false
    }} title="Nouvel article" />
};

export default Page
