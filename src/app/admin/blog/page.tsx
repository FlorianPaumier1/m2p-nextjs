"use client"
import * as React from 'react';
import TableContainer from "@/components/tables/TableContainer";
import {v4} from "uuid";
import Palmares from "@/repository/Palmares";
import CardWrapper from "@/components/layout/CardWrapper";


const Page = () => {

    return (
        <section className="w-full h-full">
        <TableContainer
                actions={['search', 'new', 'edit', 'show', 'delete']}
                headers={['Titre', 'Caption', 'Publier', 'Créer le']}
                fields={['title', 'caption', 'published', 'created_at']}
                sort={[true, true, true]}
                itemName="Articles"
                url="/admin/blog"
                model={"blog"}
                key={v4()}
            />
        </section>
    );
};

export default Page
