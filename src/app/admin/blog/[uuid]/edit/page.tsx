import * as React from 'react';
import BlogForm from "@/components/form/views/BlogForm";
import Blog from "@/repository/Blog";

type Props = {
    params: { uuid: string }
};


const Page = async ({params}: Props) => {

    const article = await Blog.find(params.uuid) ?? {}

    return <BlogForm article={article} title="Editer l'article" />
};

export default Page
