import * as React from 'react';
import Sponsor from "@/repository/Sponsor";
import SponsorForm from "@/components/form/views/SponsorForm";

type Props = {
    params: { uuid: string }
};


const Page = async ({params}: Props) => {

    const article = await Sponsor.find(params.uuid)

    return <SponsorForm item={article} title="Editer le sponsor" />
};

export default Page
