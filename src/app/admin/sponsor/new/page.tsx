import * as React from 'react';
import PalmaresForm from "@/components/form/views/PalmaresForm";
import SponsorForm from "@/components/form/views/SponsorForm";

type Props = {

};


const Page = async () => {

    return <SponsorForm item={{
        uuid: null,
        name: "",
        date: new Date(),
        isHighlighted: false,
        description: "",
        thumbnail_id: "",
        thumbnail: {
            path: "",
            uuid: ""
        }
    }} title="Nouveau Sponsor" />
};

export default Page
