"use client"
import * as React from 'react';
import TableContainer from "@/components/tables/TableContainer";
import {v4} from "uuid";
import Palmares from "@/repository/Palmares";
import CardWrapper from "@/components/layout/CardWrapper";


const Page = () => {

    return (
        <section className="w-full">
        <TableContainer
                actions={['search', 'new', 'edit', 'show', 'delete']}
                headers={['Nom', 'Date', 'Créer le']}
                fields={['name', 'date', 'created_at']}
                sort={[true, true, true]}
                itemName="Sponsor"
                url="/admin/sponsor"
                model={"sponsor"}
                key={v4()}
            />
        </section>
    );
};

export default Page
