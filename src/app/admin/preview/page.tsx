// @flow
import * as React from 'react';

type Props = {

};
const Page = (props: Props) => {
    return (
        <iframe src="/" className="w-full max-h-screen min-h-screen border"></iframe>
    );
};

export default Page
