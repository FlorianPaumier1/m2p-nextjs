// @flow
import * as React from 'react';
import {H1} from "@/components/ui/typographie";
import {db} from "@/repository/prisma";
import Settings, {MenuType} from "@/repository/Settings";
import {Input} from "@/components/ui/input";
import {Switch} from "@/components/ui/switch";
import FormFileInput from "@/components/form/FormFileInput";
import SettingsForm from "@/components/form/views/SettingsForm";
import About, {AboutType} from "@/repository/About";
import AboutForm from "@/components/form/views/AboutForm";

type Props = {

};
const Page = async (props: Props) => {

    const config = await About.findFirst()

    const handleSubmit = async (values: Partial<AboutType>) => {
        "use server"
        return About.persist(values)
    }

    return (
        <div>
            <H1 center={true}>
                Configuration du site
            </H1>
            <AboutForm about={config} handleSubmit={handleSubmit}/>
        </div>
    );
};

export default Page
