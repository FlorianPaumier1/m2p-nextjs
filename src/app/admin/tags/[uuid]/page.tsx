// @flow
import * as React from 'react';
import {db} from "@/repository/prisma";
import {H1} from "@/components/ui/typographie";
import {Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle} from "@/components/ui/card";
import CardWrapper from "@/components/layout/CardWrapper";
import Image from "next/image";
import moment from "moment";
import PalmaresCard from "@/components/ui/PalmaresCard";
import Link from "next/link";
import SponsorsCard from "@/components/ui/SponsorsCard";

type Props = {
    params: {
        uuid: string
    }
};
const Page = async ({params: {uuid}}: Props) => {

    const sponsor = await db.sponsor.findUnique({
        select: {
            name: true,
            description: true,
            date: true,
            thumbnail: {
                select: {
                    path: true,
                    alt: true,
                }
            }
        },
        where: {
            uuid: uuid
        }
    })

    return (
        <div className="w-full">
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/sponsor"}
            >
                Retour
            </Link>
            <H1 center={true}>Visualisation d'un sponsor</H1>
            <div className="mt-8 relative flex justify-center">
                <SponsorsCard sponsor={sponsor} />
            </div>
        </div>
    );
};

export default Page
