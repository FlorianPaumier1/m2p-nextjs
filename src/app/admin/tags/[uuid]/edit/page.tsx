import * as React from 'react';
import Sponsor from "@/repository/Sponsor";
import SponsorForm from "@/components/form/views/SponsorForm";
import Tag from "@/repository/Tag";
import TagForm from "@/components/form/views/TagForm";

type Props = {
    params: { uuid: string }
};


const Page = async ({params}: Props) => {

    const article = await Tag.find(params.uuid)

    return <TagForm item={article} title="Editer le tag" />
};

export default Page
