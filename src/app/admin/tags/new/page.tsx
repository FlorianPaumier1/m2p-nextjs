import * as React from 'react';
import SocialForm from "@/components/form/views/SocialForm";
import TagForm from "@/components/form/views/TagForm";


const Page = async () => {

    return <TagForm item={{
        name: ""
    }} title="Nouveau Tag" />
};

export default Page
