import * as React from 'react';
import SocialForm from "@/components/form/views/SocialForm";


const Page = async () => {

    return <SocialForm item={{
        name: "",
        icon: "",
        url: ""
    }} title="Nouveau Réseau Social" />
};

export default Page
