"use client"
import * as React from 'react';
import TableContainer from "@/components/tables/TableContainer";
import {v4} from "uuid";
import Palmares from "@/repository/Palmares";
import CardWrapper from "@/components/layout/CardWrapper";


const Page = () => {

    return (
        <section className="w-full">
        <TableContainer
                actions={['search', 'new', 'edit', 'delete']}
                headers={['Nom', 'Date']}
                fields={['name', 'date']}
                sort={[true, true]}
                itemName="Réseau social"
                url="/admin/social"
                model={"social"}
                key={v4()}
            />
        </section>
    );
};

export default Page
