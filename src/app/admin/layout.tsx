// @flow
import * as React from 'react';
import {ReactElement} from "react";
import Sidebar from "@/components/layout/Sidebar";
import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

type Props = {
    children: ReactElement
};
const Layout = async ({children, ...props}: Props) => {

    return (
        <div className="flex flex-row bg-secondary justify-center items-start min-h-screen">
            <section className="sm:w-72">
                <Sidebar />
            </section>
            <section className="w-full xl:w-2/3 md:w-5/6 mx-auto p-4 sm:p-8 bg-white shadow-xl my-auto ">
                {children}
            </section>
            <ToastContainer />
        </div>
    );
};

export default Layout
