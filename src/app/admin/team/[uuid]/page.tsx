// @flow
import * as React from 'react';
import Team from "@/repository/Team";
import {H1} from "@/components/ui/typographie";
import PeopleContainer from "@/components/ui/team/PeopleContainer";
import Link from "next/link";

type Props = {
    params: { uuid: string }
};
const Page = async (props: Props) => {

    const teams = await Team.findAll()

    return (
        <div className="w-full">
            <Link
                type="button"
                className="bg-secondary px-4 py-2 rounded"
                href={"/admin/team"}
            >
                Retour
            </Link>
            <H1 center={true}>Visulation de l'équipe</H1>
            <section className="container">
                <PeopleContainer teams={teams} />
            </section>
        </div>
    );
};

export default Page
