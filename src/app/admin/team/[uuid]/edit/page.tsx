import * as React from 'react';
import BlogForm from "@/components/form/views/BlogForm";
import Blog from "@/repository/Blog";
import Team from "@/repository/Team";
import {redirect} from "next/navigation";
import TeamForm from "@/components/form/views/TeamForm";

type Props = {
    params: { uuid: string }
};


const Page = async ({params}: Props) => {

    const article = await Team.find(params.uuid)

    if (!article) redirect("/admin/team/new")

    return <TeamForm team={article} title="Editer la personne" />
};

export default Page
