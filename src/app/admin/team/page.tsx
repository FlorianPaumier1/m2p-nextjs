"use client"
import * as React from 'react';
import TableContainer from "@/components/tables/TableContainer";
import {v4} from "uuid";
import Palmares from "@/repository/Palmares";
import CardWrapper from "@/components/layout/CardWrapper";


const Page = () => {

    return (
        <section className="w-full h-full">
        <TableContainer
                actions={['search', 'new', 'edit', 'show', 'delete']}
                headers={['Nom Prénom', 'Position', 'Ordre']}
                fields={['full_name', 'position', 'sort']}
                sort={[true, true, true]}
                itemName="Personne"
                url="/admin/team"
                model={"team"}
                key={v4()}
            />
        </section>
    );
};

export default Page
