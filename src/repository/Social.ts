import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type SocialType = {
    uuid?: string,
    icon?: string,
    name?: string,
    url?: string,
}


class Social implements Repository {

    getIdField() {
        return "uuid"
    }

    async getList({
                      currentPageNumber,
                      itemNumberPerPage,
                      sort,
                      search
                  }: listPagination): Promise<SocialType[]> {

        return db.social.findMany({
            select: {
                uuid: true,
                name: true,
                url: true,
                icon: true
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.social.count({
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.social.delete({
            where: {
                uuid
            }
        })
    }

    find(uuid: string) {
        return db.social.findUnique({
            select: {
                uuid: true,
                name: true,
                url: true,
                icon: true
            },
            where: {
                uuid: uuid
            }
        })
    }

    async persist(data: SocialType): Promise<{data: any, success: boolean}> {
        let element = null

        try {
            if (data.uuid) {
                element = await db.social.update({
                    data: data,
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.social.create({
                    data: {
                        name: data.name,
                        url: data.url,
                        icon: data.icon
                    }
                })
            }
        } catch (e) {
            return {data: e, success: false}
        }

        return {data: element, success: true}
    }
}

const social = new Social()

export default social
