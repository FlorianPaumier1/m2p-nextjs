import {db} from "@/repository/prisma";
import {FileType} from "@/repository/File";

export type HomeType = {
    uuid?: string,
    title?: string,
    caption?: string,
    banner_id?: string,
    banner?: Partial<FileType>,
    hero_text?: Partial<HomeTextType>[]
}

export type HomeTextType = {
    uuid?: string,
    content?: string,
    sort?: number
}

export type HomeAboutType = {
    uuid?: string,
    title?: string,
    content?: string,
    sort?: number,
    home_id?: string
}

export const getHome = async () => {
    return db.home.findFirst({
        select: {
            uuid: true,
            title: true,
            caption: true,
            banner_id: true,
            banner: {
                select: {
                    uuid: true,
                    path: true
                }
            },
            hero_text: {
                select: {
                    uuid: true,
                    content: true,
                    sort: true
                }
            }
        }
    })
}

export const findHomeAbout = async (home?: string) => {
    return db.home_about.findMany({
        where: {
            home_id: home
        }
    })
}

export const persistHome = async (home: HomeType): Promise<HomeType> => {

    if (home.uuid) {
        return db.home.update({
            where: {uuid: home.uuid},
            data: {
                title: home.title,
                caption: home.caption,
                banner_id: home.banner_id
            }
        })
    }

    return db.home.create({
        data: {
            title: home.title,
            caption: home.caption,
            banner_id: home.banner_id
        }
    })
}

export const persistTextBanner = async (home: string, values: HomeTextType[]) => {
    const texts = await db.hero_text_line.findMany({
        where : {
            home_id: home
        }
    })

    const toDelete = texts.filter(text => !values.find(value => value.uuid === text.uuid))
    const toUpdate = texts.filter(text => values.find(value => value.uuid === text.uuid))
    const toCreate = values.filter(value => !value.uuid)

    await db.hero_text_line.deleteMany({
        where : {
            uuid: {
                in: toDelete.map(text => text.uuid)
            }
        }
    })

    for (const toUpdateElement of toUpdate) {
        await db.hero_text_line.update({
            where: {uuid: toUpdateElement.uuid},
            data: {
                content: toUpdateElement.content,
                sort: toUpdateElement.sort
            }
        })
    }

    for (const toCreateElement of toCreate) {
        await db.hero_text_line.create({
            data: {
                content: toCreateElement.content,
                sort: toCreateElement.sort,
                home_id: home
            }
        })
    }
}
