import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type BlogType = {
    uuid?: string,
    slug?: string,
    thumbnail_id?: string,
    thumbnail?: FileType,
    banner_id?: string,
    banner?: FileType,
    title?: string,
    content?: string,
    caption?: string,
    published?: boolean,
    tags?: TagBlog[],
    created_at?: Date
}

export type TagBlog = {
    uuid: string,
    name: string,
}

class Blog implements Repository {

    getIdField() {
        return "uuid"
    }

    async getList({
                      currentPageNumber,
                      itemNumberPerPage,
                      sort,
                      search
                  }: listPagination): Promise<BlogType[]> {

        return db.blog.findMany({
            select: {
                uuid: true,
                title: true,
                caption: true,
                published: true,
                created_at: true
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                title: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.blog.count({
            where: {
                title: {
                    contains: search
                }
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.blog.delete({
            where: {
                uuid
            }
        })
    }

    find(uuid: string) {
        return db.blog.findUnique({
            select: {
                uuid: true,
                title: true,
                caption: true,
                content: true,
                thumbnail_id: true,
                banner_id: true,
                thumbnail: {
                    select: {
                        uuid: true,
                        path: true
                    }
                },
                banner: {
                    select: {
                        uuid: true,
                        path: true
                    }
                },
                tags: {
                    select: {
                        uuid: true
                    }
                }
            },
            where: {
                uuid: uuid
            }
        })
    }

    async persist(data: any): Promise<{ data: any, success: boolean }> {
        let element = null
        data.slug = data.title.toLowerCase().replace(/ /g, "-")

        const tags = data.tags.map((tag_id: string) => {
            return {uuid: tag_id}
        })

        try {
            if (data.uuid) {
                element = await db.blog.update({
                    data: {
                        title: data.title,
                        content: data.content,
                        caption: data.caption,
                        slug: data.slug,
                        thumbnail_id: data.thumbnail_id,
                        banner_id: data.banner_id,
                        published: data.published,
                        tags: {
                            connect: tags
                        }
                    },
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.blog.create({
                    data: {
                        title: data.title,
                        content: data.content,
                        caption: data.caption,
                        slug: data.slug,
                        thumbnail_id: data.thumbnail_id,
                        banner_id: data.banner_id,
                        published: data.published,
                        tags: {
                            connect: tags
                        }
                    }
                })
            }
        } catch (e) {
            return {data: e, success: false}
        }

        return {data: element, success: true}
    }
}

export default new Blog()
