import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type AboutType = {
    uuid?: string,
    banner_id?: string,
    banner?: FileType,
    content?: string
}


class About {

    findFirst() {
        return db.about.findFirst({
            select: {
                uuid: true,
                content: true,
                banner_id: true,
                banner: {
                    select: {
                        uuid: true,
                        path: true
                    }
                }
            }
        })
    }

    async persist(data: any): Promise<{data: any, success: boolean}> {
        let element = null

        try {
            if (data.uuid) {
                element = await db.about.update({
                    data: data,
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.about.create({
                    data: {
                        banner_id: data.banner_id,
                        content: data.content,
                    }
                })
            }
        } catch (e) {
            return {data: e, success: false}
        }

        return {data: element, success: true}
    }
}

export default new About()
