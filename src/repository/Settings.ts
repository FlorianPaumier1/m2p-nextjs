import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type SettingsType = {
    uuid?: string,
    logo_id?: string,
    logo?: FileType,
    title?: string,
    maintenance?: boolean
}


class Settings {

    findFirst() {
        return db.configuration.findFirst({
            select: {
                uuid: true,
                title: true,
                maintenance: true,
                logo_id: true,
                logo: {
                    select: {
                        uuid: true,
                        path: true
                    }
                }
            }
        })
    }

    async persist(data: any): Promise<{data: any, success: boolean}> {
        let element = null

        try {
            if (data.uuid) {
                element = await db.configuration.update({
                    data: data,
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.configuration.create({
                    data: {
                        title: data.title,
                        maintenance: data.maintenance,
                        logo_id: data.logo_id,
                    }
                })
            }
        } catch (e) {
            return {data: e, success: false}
        }

        return {data: element, success: true}
    }
}

export default new Settings()
