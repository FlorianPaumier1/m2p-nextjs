import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type TagType = {
    uuid?: string,
    name: string
}


class Tag implements Repository {

    getIdField() {
        return "uuid"
    }

    async getList({
                      currentPageNumber,
                      itemNumberPerPage,
                      sort,
                      search
                  }: listPagination): Promise<TagType[]> {

        return db.tag.findMany({
            select: {
                uuid: true,
                name: true
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.tag.count({
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.tag.delete({
            where: {
                uuid: uuid
            }
        })
    }

    find(uuid: string) {
        return db.tag.findUnique({
            select: {
                uuid: true,
                name: true
            },
            where: {
                uuid: uuid
            }
        })
    }

    async persist(data: TagType): Promise<{data: any, success: boolean}> {
        let element = null
        try {
            if (data.uuid) {
                element = await db.tag.update({
                    data: data,
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.tag.create({
                    data: {
                        name: data.name,
                    }
                })
            }
        } catch (e) {
            return {data: e, success: false}
        }

        return {data: element, success: true}
    }

    async findListSelect(): Promise<{label: string, value: string}[]> {
        const tags = await db.tag.findMany({})
        return tags.map((tag) => {
            return {
                label: tag.name,
                value: tag.uuid
            }
        })
    }
}

const tag = new Tag()
export default tag
