import {PrismaClient} from '@prisma/client'

declare global {
    var prisma: PrismaClient | undefined
}

export const db = globalThis.prisma || new PrismaClient()

if (process.env.NODE_ENV !== 'production') globalThis.prisma = db

export interface Repository {
    getList: (listPagination: listPagination) => Promise<any[]>,
    count: (search: string) => Promise<number>,
    delete: (uuid: string) => Promise<void>,
    persist: (value: any) => Promise<{success: boolean, data: any}>,
    getIdField: () => string
}

export type listPagination = {
    currentPageNumber: number,
    itemNumberPerPage: number,
    sort: {
        field: string,
        direction: string
    },
    search: string
}
