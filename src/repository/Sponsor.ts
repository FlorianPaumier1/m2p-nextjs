import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type SponsorType = {
    uuid?: string,
    thumbnail_id?: string,
    thumbnail?: Partial<FileType>,
    name?: string,
    description?: string,
    isHighlighted?: boolean,
    date: Date,
    created_at?: Date
}


class Sponsor implements Repository {

    getIdField() {
        return "uuid"
    }

    async getList({
                      currentPageNumber,
                      itemNumberPerPage,
                      sort,
                      search
                  }: listPagination): Promise<SponsorType[]> {

        return db.sponsor.findMany({
            select: {
                uuid: true,
                name: true,
                date: true,
                description: true,
                created_at: true
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.sponsor.count({
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.sponsor.delete({
            where: {
                uuid
            }
        })
    }

    find(uuid: string) {
        return db.sponsor.findUnique({
            select: {
                uuid: true,
                name: true,
                description: true,
                date: true,
                created_at: true,
                isHighlighted: true,
                thumbnail_id: true,
                thumbnail: {
                    select: {
                        uuid: true,
                        path: true
                    }
                }
            },
            where: {
                uuid: uuid
            }
        })
    }

    async persist(data: any): Promise<{data: any, success: boolean}> {
        let element = null

        try {
            if (data.uuid) {
                element = await db.sponsor.update({
                    data: data,
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.sponsor.create({
                    data: {
                        date: data.date,
                        description: data.description,
                        name: data.name,
                        thumbnail_id: data.thumbnail_id,
                        isHighlighted: data.isHighlighted
                    }
                })
            }
        } catch (e) {
            return {data: e.message, success: false}
        }

        return {data: element, success: true}
    }
}

const sponsor = new Sponsor();
export default sponsor
