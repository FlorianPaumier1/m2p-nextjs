"use server";

import {IPagination} from "@/components/tables/Pagination";
import {Repository} from "@/repository/prisma";
import User from "@/repository/User";
import Palmares from "@/repository/Palmares";
import Menu from "@/repository/Menu";
import Blog from "@/repository/Blog";
import Sponsor from "@/repository/Sponsor";
import Social from "@/repository/Social";
import Tag from "@/repository/Tag";
import Contact from "@/repository/Contact";
import Team from "@/repository/Team";


const getModel = (modelName: string) => {

    switch (modelName) {
        case "user":
           return User
        case "blog":
           return Blog
        case "contact":
           return Contact
        case "palmares":
           return Palmares
        case "sponsor":
           return Sponsor
        case "tags":
           return Tag
        case "menu":
           return Menu
        case "team":
           return Team
        case "social":
           return Social
    }

    return null;
}

export const fetchData = async (modelName: string, pagination: {
    currentPageNumber: number,
    itemNumberPerPage: number,
    sortItem: { field: string, direction: string },
    search: string
}): Promise<IPagination> => {

    let model: Repository | null = getModel(modelName);

    let data = [];
    let totalCount = 0;
    let idField = "uuid";

    if (model) {
        data = await model.getList({
            currentPageNumber: pagination.currentPageNumber,
            itemNumberPerPage: pagination.itemNumberPerPage,
            sort: pagination.sortItem,
            search: pagination.search
        })

        totalCount = await model.count(pagination.search)
        idField = model.getIdField()
    }

    return {
        totalItemCount: totalCount,
        pageCount: Math.ceil(totalCount / pagination.itemNumberPerPage),
        items: data,
        currentPageNumber: 1,
        itemNumberPerPage: pagination.itemNumberPerPage,
        idField: idField
    }
}

export const findById = async (modelName: string, id: string) => {

}

export const deleteItem = async (modelName: string, id: string) => {
    const model = getModel(modelName);
    await model.delete(id)
}

export const findTagListSelect = async () => await Tag.findListSelect()
