import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type ContactType = {
    uuid?: string,
    name: string,
    email: string,
    message: string,
    subject: string
}

class Contact implements Repository {

    getIdField() {
        return "uuid"
    }

    async getList({
                      currentPageNumber,
                      itemNumberPerPage,
                      sort,
                      search
                  }: listPagination): Promise<ContactType[]> {

        return db.contact.findMany({
            select: {
                uuid: true,
                name: true,
                email: true,
                subject: true,
                message: true
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.contact.count({
            where: {
                name: {
                    contains: search
                },
                subject: {
                    contains: search
                },
                email: {
                    contains: search
                }
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.contact.delete({
            where: {
                uuid
            }
        })
    }

    find(uuid: string) {
        return db.contact.findUnique({
            select: {
                uuid: true,
                name: true,
                email: true,
                subject: true,
                message: true,
                created_at: true
            },
            where: {
                uuid: uuid
            }
        })
    }

    async persist(data: any): Promise<{ data: any, success: boolean }> {
        return {data: null, success: true}
    }
}

const contact = new Contact()

export default contact
