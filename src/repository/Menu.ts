import {db, listPagination, Repository} from "@/repository/prisma";

export type MenuType = {
    uuid: string
    name: string
    label: string
    sort: number
    path?: string
    enabled: boolean
}


class Menu implements Repository {

    getIdField() {
        return "uuid"
    }

    getMenuItems = async () => {
        return await db.menu.findMany({
            where: {
                enabled: true
            },
            orderBy: {
                sort: 'asc'
            }
        })
    }

    async getList ({
                         currentPageNumber,
                         itemNumberPerPage,
                         sort,
                         search
                     }: listPagination) {
        return await db.menu.findMany({
            select: {
                uuid: true,
                name: true,
                label: true,
                sort: true,
                path: true,
                enabled: true,
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.menu.count({
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async find(uuid: string): Promise<MenuType | null> {
        return await db.menu.findUnique({
            where: {
                uuid
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.menu.delete({
            where: {
                uuid
            }
        })
    }

    persist = async (data: any) => {
        return {data: null, success: true}
    }
}

export default new Menu()
