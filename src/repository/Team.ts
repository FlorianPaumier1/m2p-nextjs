import {join, normalize} from "path"
import {writeFile} from "fs/promises";
import {db, listPagination, Repository} from "@/repository/prisma";
import {PalmaresListItem} from "@/repository/Palmares";
import {z} from "zod";
import {FileType} from "@/repository/File";

export type TeamType = {
    uuid?: string,
    thumbnail_id?: string,
    thumbnail?: Partial<FileType>,
    full_name: string,
    sort?: number,
    position: string,
    gender?: string
}


class Team implements Repository {
    findAll() {
        return db.team.findMany({
            select: {
                uuid: true,
                full_name: true,
                position: true,
                gender: true,
                thumbnail: {
                    select: {
                        path: true
                    }
                }
            },
            orderBy: {
                sort: "asc"
            }
        })
    }

    getIdField() {
        return "uuid"
    }

    async getList({
                      currentPageNumber,
                      itemNumberPerPage,
                      sort,
                      search
                  }: listPagination): Promise<TeamType[]> {

        return db.team.findMany({
            select: {
                uuid: true,
                full_name: true,
                position: true,
                sort: true,
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                full_name: {
                    contains: search
                },
                position: {
                    contains: search
                },
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.team.count({
            where: {
                full_name: {
                    contains: search
                },
                position: {
                    contains: search
                },
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.team.delete({
            where: {
                uuid
            }
        })
    }

    find(uuid: string) {
        return db.team.findUnique({
            select: {
                uuid: true,
                full_name: true,
                position: true,
                sort: true,
                thumbnail_id: true,
                thumbnail: {
                    select: {
                        uuid: true,
                        path: true
                    }
                }
            },
            where: {
                uuid: uuid
            }
        })
    }

    async persist(data: any): Promise<{ data: any, success: boolean }> {
        let element = null

        try {
            if (data.uuid) {
                element = await db.team.update({
                    data: {
                        full_name: data.full_name,
                        position: data.position,
                        sort: data.sort,
                        thumbnail_id: data.thumbnail_id,
                    },
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.team.create({
                    data: {
                        full_name: data.full_name,
                        position: data.position,
                        sort: data.sort,
                        thumbnail_id: data.thumbnail_id,
                    }
                })
            }
        } catch (e) {
            return {data: e, success: false}
        }

        return {data: element, success: true}
    }
}

const team = new Team()
export default team
