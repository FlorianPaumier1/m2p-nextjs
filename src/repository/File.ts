"use server"
import {join} from "path"
import {writeFile} from "fs/promises";
import {db} from "@/repository/prisma";
import bcrypt from "bcrypt";
import {v4} from "uuid";

export type FileType = {
    uuid: string,
    name?: string,
    size?: number,
    mime_type?: string,
    path?: string,
    is_processed?: boolean,
    type?: string,
    alt?: string
}

export async function upload(file: File) {
    'use server'

    if (!file) {
        throw new Error('No file uploaded')
    }

    const bytes = await file.arrayBuffer()
    const buffer = Buffer.from(bytes)
    const name = v4()
    console.log(name)
    const publicPath = `/media/${name}.${file.name.split('.').pop()}`

    // With the file data in the buffer, you can do whatever you want with it.
    // For this, we'll just write it to the filesystem in a new location
    const path = join(process.cwd(), "public", publicPath)
    await writeFile(path, buffer)

    return db.file.create({
        data: {
            name: file.name,
            size: file.size,
            path: publicPath,
            mime_type: file.type,
        }
    })
}
