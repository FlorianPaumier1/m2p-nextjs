import {db, db as prisma, listPagination, Repository} from "@/repository/prisma";

export type UserType = {
    email: string;
    password: string;
    id: string;
}

class User implements Repository {
    async login(username: string): Promise<UserType | null> {
        return prisma.user.findUnique({
            select: {
                email: true,
                id: true,
                password: true
            },
            where: {
                email: username
            }
        });
    }

    getIdField() {
        return "id"
    }

    getList = async ({
                         currentPageNumber,
                         itemNumberPerPage,
                         sort,
                         search
                     }: listPagination) => {
        return await db.user.findMany({
            select: {
                id: true,
                email: true,
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                email: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.user.count({
            where: {
                email: {
                    contains: search
                }
            }
        })
    }

    async delete(id: string): Promise<void> {
        await db.user.delete({
            where: {
                id
            }
        })
    }

    persist = async (data: UserType) => {
        return {data: null, success: true}
    }
}

export default new User();
