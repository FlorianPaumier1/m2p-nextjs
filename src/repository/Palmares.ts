import {listPagination, db, Repository} from "@/repository/prisma";
import {FileType} from "@/repository/File";

export type PalmaresListItem = {
    uuid: string,
    file?: FileType | null,
    name: string,
    date: Date,
    created_at: Date | null
}

export type PalamaresType = {
    uuid?: string,
    name: string,
    date: Date,
    description: string,
    thumbnail_id?: string,
    thumbnail: Partial<FileType>
}


class Palmares implements Repository {

    getIdField() {
        return "uuid"
    }

    async getList({
                      currentPageNumber,
                      itemNumberPerPage,
                      sort,
                      search
                  }: listPagination): Promise<PalmaresListItem[]> {
        return db.palmares.findMany({
            select: {
                uuid: true,
                name: true,
                date: true,
                created_at: true
            },
            orderBy: {
                [sort.field]: sort.direction
            },
            skip: (currentPageNumber - 1) * itemNumberPerPage,
            take: itemNumberPerPage,
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async count(search: string): Promise<number> {
        return db.palmares.count({
            where: {
                name: {
                    contains: search
                }
            }
        })
    }

    async delete(uuid: string): Promise<void> {
        await db.palmares.delete({
            where: {
                uuid
            }
        })
    }

    find(uuid: string): Promise<PalamaresType> {
        return db.palmares.findUnique({
            select: {
                uuid: true,
                name: true,
                date: true,
                description: true,
                thumbnail_id: true,
                thumbnail: {
                    select: {
                        uuid: true,
                        path: true
                    }
                }
            },
            where: {
                uuid: uuid
            }
        })
    }

    async persist(data: any) {
        let element = null
        try {

            if (data.uuid) {
                element = await db.palmares.update({
                    data: data,
                    where: {
                        uuid: data.uuid
                    }
                })
            } else {
                element = await db.palmares.create({
                    data: {
                        name: data.name,
                        date: data.date,
                        description: data.description,
                        thumbnail_id: data.thumbnail_id
                    }
                })
            }
        }catch (e) {
            return {success: false, data: e}
        }

        return {success: true, data: element}
    }
}


export default new Palmares()
