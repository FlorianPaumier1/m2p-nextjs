import {NextRequest} from "next/server";
import NextAuth from "next-auth";
import AuthConfig from "@/auth.config";
import {apiAuthRoute} from "@/lib/route";

const { auth} = NextAuth(AuthConfig)

export default auth((req: NextRequest) => {
    const route = req.nextUrl.pathname

    console.log(route)
    // @ts-ignore
    const isLoggedIn = req.auth

    const isAuthRoute = route.startsWith(apiAuthRoute)
    const isAdminRoute = route.startsWith("/admin") || route.startsWith("/api/admin")

    if (isAdminRoute && !isLoggedIn){
        return Response.redirect(new URL("/auth/login", req.nextUrl))
    }
})

// Optionally, don't invoke Middleware on some paths
// Read more: https://nextjs.org/docs/app/building-your-application/routing/middleware#matcher
export const config = {
    matcher: ["/((?!.+\\.[\\w]+$|_next).*)", "/", "/(api|trpc)(.*)"],
};
